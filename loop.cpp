// main loop and some support routines

#define EXTERN
#include "defines.h"  // various globals
#include "src/JsonParser.h"
#include "DAC.h"
// #include "src/AD7689.h"         // external ADC // this
#include "eeprom.h"
#include "serial.h"
#include "flasher.h"
#include "src/crc32.h"
#include <TimeLib.h>
#include "util.h"
#include "malloc.h"
// #include <i2c_t3.h>
// #include "src/TCS3471.h"              // color sensor // this
#include <SPI.h>

#define TIA
#define YESBATT  // set to 0
// #define NOBAT // set to this if the device does not use a battery (old co2 sensor)

// function declarations

inline static void startTimers(unsigned _pulsedistance);
inline static void stopTimers(void);
void reset_freq(void);
void upgrade_firmware(void);  // for over-the-air firmware updatesmil
void boot_check(void);        // for over-the-air firmware updates
//int get_light_intensity(int x);
static void recall_save(JsonArray _recall_eeprom, JsonArray _save_eeprom);
void get_set_device_info(const int _set);
void temp_get_set_device_info();
int abort_cmd(void);
static void environmentals(JsonArray a, const int _averages, const int x, int oneOrArray);
// void readSpectrometer(int intTime, int delay_time, int read_time, int accumulateMode);
// void MAG3110_read(int* x, int* y, int* z);
// void MMA8653FC_read(int* axeXnow, int* axeYnow, int* axeZnow);
// void MMA8653FC_standby(void);
// float MLX90615_Read(int TaTo);
uint16_t par_to_dac(float _par, uint16_t _pin);
// float light_intensity_raw_to_par (float _light_intensity_raw, float _r, float _g, float _b);
// unsigned long requestCo2(int timeout);
static void print_all(void);
float expr(const char str[]);
void do_protocol(void);
void do_command(void);
void print_calibrations(void);
// void start_on_open_close(void);
// void start_on_pin_high(int pin);
//void get_compass_and_angle (int notRaw, int _averages);
//float get_thickness (int notRaw, int _averages);
// float get_contactless_temp (int _averages);
void get_detector_value(int _averages, int this_light, int this_intensity, int this_detector, int this_pulsesize, int detector_read1or2or3);
// void get_detector_value_test(int _averages, int this_pulses, int this_intensity, int this_detector, int this_pulsesize, int this_pulse_distance);
void get_temperature_humidity_pressure_voc(int _averages);
// void get_temperature_humidity_pressure2 (int _averages);
void init_chips(void);
void configure_bluetooth(void);
void reboot(void);
void turn_on_3V3(void);
void turn_on_5(void);
//void constant_light (void);

struct theReadings {  // use to return which readings are associated with the environmental_array calls
  const char* reading1;
  const char* reading2;
  const char* reading3;
  const char* reading4;
  const char* reading5;
  int numberReadings;
};
theReadings getReadings(const char* _thisSensor);  // get the actual sensor readings associated with each environmental call (so compass and angle when you call compass_and_angle, etc.)

//////////////////////// MAIN LOOP /////////////////////////

// process ascii serial input commands of two forms:
// 1010+<parameter1>+<parameter2>+...  (a command)
// [...] (a json protocol to be executed)

void loop() {

  if (eeprom->shutdownTime < 20000 || eeprom->shutdownTime > (120 * 60 * 1000)) {  // if less than 20 seconds or greater than 2 hours, set to default (7 minutes)... this should take care of the startup state before eeprom is set manually)
    store(shutdownTime, (7 * 60 * 1000));
  }

  // read until we get a character - primary idle loop
  int c;

  activity();  // record fact that we have seen activity (used with powerdown()), make sure it's recorded after the latest protocol or command is completed.

  for (;;) {
    c = Serial_Peek();

    if (c != -1)  // received something
      break;

    powerdown();  // power down if no activity for x seconds

  }  // for

  crc32_init();

  if (c == '[')
    do_protocol();  // start of json
  else
    do_command();  // received a non '[' char - processs command

  Serial_Flush_Output();

}  // loop()

// =========================================

// globals - try to avoid

static uint8_t _meas_light;          // measuring light to be used during the interrupt
static uint16_t _pulsesize;          // pulse width in usec
static uint32_t _led_voltage;        // pulse width in usec
static volatile int pulse_done = 0;  // set by ISR

// process a numeric + command

void do_command() {
  char choose[50];
  Serial_Input_Chars(choose, "+", 500, sizeof(choose) - 1);

  for (unsigned i = 0; i < strlen(choose); ++i) {  // remove ctrl characters
    if (!isprint(choose[i]))
      choose[i] = 0;
  }

  if (strlen(choose) < 3) {  // short or null command, quietly ignore it
    return;
  }

  if (!isalnum(choose[0])) {
    Serial_Printf("{\"error\":\" bad command\"}\n");
    return;  // go read another command
  }

  unsigned val;  // we accept int or alpha commands
  if (isdigit(choose[0]))
    val = atoi(choose);
  else
    val = hash(choose);  // convert alpha command to an int

  // process single commands
  switch (val) {

    case hash("hello"):
    case 1000:  // print "Ready" to USB and/or Bluetooth
      Serial_Print(DEVICE_NAME);
      Serial_Print_Line(" Ready");
      break;

    case hash("set_dac"):
      Serial_Print("on 5v, ");
      turn_on_5V();  // is normally off, but many of the below commands need it
      // standard startup routine for new device
      DAC_set_address(LDAC1, 0, 1);  // Set DAC addresses to 1,2,3 assuming addresses are unset and all are factory (0,0,0)
      Serial_Print("dac 1, ");
      DAC_set_address(LDAC2, 0, 2);
      Serial_Print("dac 2, ");
      DAC_set_address(LDAC3, 0, 3);
      Serial_Print_Line("dac 3");
      get_set_device_info(1);  //  input device info and write to eeprom
      break;

      // case hash("constant_light"):
      //   turn_on_5V();                     // is normally off, but many of the below commands need it
      //   Serial_Print("Starting constant light source");
      //   constant_light();
      //   break;

    // case hash("cycle5v"):
    //   Serial_Print_Line("turning off 5v in 3 seconds...");
    //   delay(3000);
    //   turn_off_5V();
    //   Serial_Print_Line("turning off 3.3v in 3 seconds...");
    //   delay(3000);
    //   turn_off_3V3();
    //   Serial_Print_Line("turning on 3.3v in 3 seconds...");
    //   delay(3000);
    //   turn_on_3V3();
    //   Serial_Print_Line("turning on 5v in 3 seconds...");
    //   delay(3000);
    //   turn_on_5V();
    //   delay(3000);
    //   Serial_Print_Line("reboot before rerunning as states may be weird now");
    //   break;

    case hash("all_sensors"):                                                                          // continuously output until user enter -1+
      {
          bme.begin(0x76);

          float temperature1 = bme.readTemperature();
          float relative_humidity1 = bme.readHumidity();
          float pressure1 = bme.readPressure() / 100;
          float voc1 = (float)bme.readGas() / 1000; // measured in KOhms

          Serial_Printf("{\"temperature\":%f,\"relative_humidity\":%f,\"pressure\":%f,\"gas\":%f}", temperature1, relative_humidity1, pressure1, voc1);
          Serial_Print_CRC();

      }
      break;

    case hash("any_light"):
      {
        turn_on_5V();  // turn on 5V to turn on the lights
        Serial_Print_Line("\"message\": \"Enter led # setting followed by +: \"}");
        int led = Serial_Input_Double("+", 0);
        Serial_Print_Line("\"message\": \"Enter dac setting followed by +:  \"}");
        int setting = Serial_Input_Double("+", 0);
        DAC_set(led, setting);
        DAC_change();
        digitalWriteFast(PULSE_OFF, HIGH);
        delay(1000);
        digitalWriteFast(PULSE_OFF, LOW);
        DAC_set(led, 0);
        DAC_change();
      }
      break;

    case hash("print_memory"):
      print_calibrations();
      break;

    case hash("device_info"):
    case 1007:
      get_set_device_info(0);
      break;

    case hash("set_device_info"):  // set the device name and
    case 1008:
      get_set_device_info(1);
      break;

    case hash("configure_bluetooth"):  // set the bluetooth name and baud rate
      configure_bluetooth();
      break;

    case hash("testAllLights"):
      {
        turn_on_5V();  // turn on 5V to turn on the lights
        int order[10] = { 1, 2, 3, 10, 4, 5, 6, 7, 8, 9 };
        for (int i = 0; i < 10; i++) {
          Serial_Printf("PULSE%d\n", order[i]);
          DAC_set(order[i], 50);
          DAC_change();
          digitalWriteFast(PULSE_OFF, HIGH);
          delay(500);
          digitalWriteFast(PULSE_OFF, LOW);
          DAC_set(order[i], 0);
          DAC_change();
        }
      }
      break;
    case hash("light1"):
      turn_on_5V();  // turn on 5V to turn on the lights
      Serial_Print_Line("PULSE1");
      DAC_set(1, 50);
      DAC_change();
      digitalWriteFast(PULSE_OFF, HIGH);
      delay(750);
      digitalWriteFast(PULSE_OFF, LOW);
      DAC_set(1, 0);
      DAC_change();
      break;
    case hash("light2"):
      turn_on_5V();  // turn on 5V to turn on the lights
      Serial_Print_Line("PULSE2");
      DAC_set(2, 50);
      DAC_change();
      digitalWriteFast(PULSE_OFF, HIGH);
      delay(750);
      digitalWriteFast(PULSE_OFF, LOW);
      DAC_set(2, 0);
      DAC_change();
      break;
    case hash("light3"):
      turn_on_5V();  // turn on 5V to turn on the lights
      Serial_Print_Line("PULSE3");
      DAC_set(3, 50);
      DAC_change();
      digitalWriteFast(PULSE_OFF, HIGH);
      delay(750);
      digitalWriteFast(PULSE_OFF, LOW);
      DAC_set(3, 0);
      DAC_change();
      break;
    case hash("light4"):
      turn_on_5V();  // turn on 5V to turn on the lights
      Serial_Print_Line("PULSE4");
      DAC_set(4, 75);
      DAC_change();
      digitalWriteFast(PULSE_OFF, HIGH);
      delay(750);
      digitalWriteFast(PULSE_OFF, LOW);
      DAC_set(4, 0);
      DAC_change();
      break;
    case hash("light5"):
      turn_on_5V();  // turn on 5V to turn on the lights
      Serial_Print_Line("PULSE5");
      DAC_set(5, 75);
      DAC_change();
      digitalWriteFast(PULSE_OFF, HIGH);
      delay(750);
      digitalWriteFast(PULSE_OFF, LOW);
      DAC_set(5, 0);
      DAC_change();
      break;
    case hash("light6"):
      turn_on_5V();  // turn on 5V to turn on the lights
      Serial_Print_Line("PULSE6");
      DAC_set(6, 75);
      DAC_change();
      digitalWriteFast(PULSE_OFF, HIGH);
      delay(750);
      digitalWriteFast(PULSE_OFF, LOW);
      DAC_set(6, 0);
      DAC_change();
      break;
    case hash("light7"):
      turn_on_5V();  // turn on 5V to turn on the lights
      Serial_Print_Line("PULSE7");
      DAC_set(7, 75);
      DAC_change();
      digitalWriteFast(PULSE_OFF, HIGH);
      delay(750);
      digitalWriteFast(PULSE_OFF, LOW);
      DAC_set(7, 0);
      DAC_change();
      break;
    case hash("light8"):
      turn_on_5V();  // turn on 5V to turn on the lights
      Serial_Print_Line("PULSE8");
      DAC_set(8, 75);
      DAC_change();
      digitalWriteFast(PULSE_OFF, HIGH);
      delay(750);
      digitalWriteFast(PULSE_OFF, LOW);
      DAC_set(8, 0);
      DAC_change();
      break;
    case hash("light9"):
      turn_on_5V();  // turn on 5V to turn on the lights
      Serial_Print_Line("PULSE9");
      DAC_set(9, 75);
      DAC_change();
      digitalWriteFast(PULSE_OFF, HIGH);
      delay(750);
      digitalWriteFast(PULSE_OFF, LOW);
      DAC_set(9, 0);
      DAC_change();
      break;
    case hash("light10"):
      turn_on_5V();  // turn on 5V to turn on the lights
      Serial_Print_Line("PULSE10");
      DAC_set(10, 50);
      DAC_change();
      digitalWriteFast(PULSE_OFF, HIGH);
      delay(750);
      digitalWriteFast(PULSE_OFF, LOW);
      DAC_set(10, 0);
      DAC_change();
      break;

    case hash("set_serial"):
      {

        Serial_Print("Enter 1/2/3/4+\n");
        long setserial = Serial_Input_Long();
        Serial_Printf("set serial to %d\n", (int)setserial);
        Serial_Set((int)setserial);
        Serial_Print_Line("test print");
      }
      break;

    case hash("reset"):
    case 1027:  // restart teensy (keep here!)
      _reboot_Teensyduino_();
      break;

    case hash("reboot"):
      reboot();
      break;

    case hash("print_all"):
    case 1029:
      print_all();  // print everything in the eeprom (all values defined in eeprom.h)
      break;

    case hash("set_shutdown"):
      {
        long shutdownTemp = Serial_Input_Long("+", 0);
        if (shutdownTemp > 20000 && shutdownTemp < (120 * 60 * 1000)) {  // if the inputted values are in range (20 sec --> 2 hours) then save them
          store(shutdownTime, shutdownTemp);
        } else {  // otherwise, save the default value (7 minutes)
          store(shutdownTime, (7 * 60 * 1000));
        }
      }
      break;

    case hash("set_detector1_offset"):
      store(detector_offset_slope[0], Serial_Input_Double("+", 0));
      store(detector_offset_yint[0], Serial_Input_Double("+", 0));
      break;
    case hash("set_detector2_offset"):
      store(detector_offset_slope[1], Serial_Input_Double("+", 0));
      store(detector_offset_yint[1], Serial_Input_Double("+", 0));
      break;
    case hash("set_detector3_offset"):
      store(detector_offset_slope[2], Serial_Input_Double("+", 0));
      store(detector_offset_yint[2], Serial_Input_Double("+", 0));
      break;
    case hash("set_detector4_offset"):
      store(detector_offset_slope[3], Serial_Input_Double("+", 0));
      store(detector_offset_yint[3], Serial_Input_Double("+", 0));
      break;

    // CALIBRATION VALUES FOR REFLECTOMETER
    case hash("set_ir_high"):
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            store(ir_high[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;
    case hash("set_ir_low"):
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            store(ir_low[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;
    case hash("set_vis_high"):
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            store(vis_high[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;
    case hash("set_vis_low"):
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            store(vis_low[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;
    case hash("set_ir_high_master"):
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            store(ir_high_master[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;
    case hash("set_ir_low_master"):
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            store(ir_low_master[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;
    case hash("set_vis_high_master"):
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            store(vis_high_master[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;
    case hash("set_vis_low_master"):
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            store(vis_low_master[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;
    case hash("set_ir_blank"):
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            store(ir_blank[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;
    case hash("set_vis_blank"):
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            store(vis_blank[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;

    case hash("set_heatcal1"):
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            store(heatcal1[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;

    case hash("set_heatcal2"):
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            store(heatcal2[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;

    case hash("set_heatcal3"):
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            store(heatcal3[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;

    case hash("set_colorcal_blanks"): // works only on firmware version 3.0.1 or earlier
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            // store(colorcal_blank1[led], Serial_Input_Double("+", 0));
            // store(colorcal_blank2[led], Serial_Input_Double("+", 0));
            // store(colorcal_blank3[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;

    case hash("set_normalized_soil"): // works only on firmware version 3.1.1 or later
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            store(normalized_soil[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;

    case hash("set_normalized_5mlcuvette"): // works only on firmware version 3.1.1 or later
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            store(normalized_5mlcuvette[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;

    case hash("set_normalized_1mlcuvette"): // works only on firmware version 3.1.1 or later
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            store(normalized_1mlcuvette[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;

    case hash("set_normalized_custom"): // works only on firmware version 3.1.1 or later
      {
        for (;;) {
          int led = Serial_Input_Double("+", 0);
          if (led == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (led > 0 || led < NUM_LEDS + 1) {
            store(normalized_custom[led], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", NUM_LEDS + 1);
          }
        }
      }
      break;


    case hash("set_user_defined"):
      {
        for (;;) {
          int userdefID = Serial_Input_Double("+", 0);
          if (userdefID == -1) {  // user can bail with -1+ setting as LED
            break;
          } else if (userdefID > 0 || userdefID < 50) {
            store(userdef[userdefID], Serial_Input_Double("+", 0));
          } else {
            Serial_Printf("\"error\": \" User entered incorrect value.  Should be between 0 and %d", (sizeof(eeprom->userdef) / sizeof(float)));
          }
        }
      }
      break;

    case hash("upgrade"):
    case 1078:  // over the air update of firmware.   DO NOT MOVE THIS!
      upgrade_firmware();
      break;

    default:
      Serial_Printf("{\"error\":\"bad command\"}\n");
      break;

  }  // switch()

}  // do_command()


// read in and execute a protocol
// example: [{"pulses": [150],"a_lights": [[3]],"a_intensities": [[50]],"pulsedistance": 1000,"m_intensities": [[125]],"pulsesize": 2,"detectors": [[3]],"meas_lights": [[1]],"protocols": 1}]<newline>

void do_protocol() {

  const int serial_buffer_size = 6500;  // max size of the incoming jsons (6500 originally)
  const int max_jsons = 15;             // max number of protocols per measurement (15 originally)
  const int MAX_JSON_ELEMENTS = 750;    // (750 originally)

  int averages = 1;     // ??
  int32_t data = 0;     // I would like to change this back to a float, but I keep getting errors with creating uint16_t versus int16_t with other variables arrrrgggghhh!
  //  float data = 0;
  //  float data_ref = 0;
  int32_t data_ref = 0;
  int act_background_light = 0;
  //static float freqtimer0;
  //static float freqtimer1;
  //static float freqtimer2;
  int measurements = 1;                     // the number of times to repeat the entire measurement (all protocols)
  unsigned long measurements_delay_ms = 0;  // number of milliseconds to wait between measurements
  unsigned long meas_number = 0;            // counter to cycle through measurement lights 1 - 4 during the run
  //unsigned long end1;
  //unsigned long start1 = millis();

  // these variables could be pulled from the JSON at the time of use... however, because pulling from JSON is slow, it's better to create a int to save them into at the beginning of a protocol run and use the int instead of the raw hashTable.getLong type call
  int _a_lights[NUM_LEDS] = {};
  int _a_intensities[NUM_LEDS] = {};
  int _a_lights_prev[NUM_LEDS] = {};
//  int act_background_light_prev = 0;
  int cycle = 0;  // current cycle number (start counting at 0!)
  int pulse = 0;  // current pulse number
  //int total_cycles;                                                           // Total number of cycles - note first cycle is cycle 0
  int meas_array_size = 0;  // measures the number of measurement lights in the current cycle (example: for meas_lights = [[15,15,16],[15],[16,16,20]], the meas_array_size's are [3,1,3].
  //int end_flag = 0;

  int number_of_protocols = 0;

  // make sure that DEBUG_DC and SS2 are set as an input pin and pulled low.  These are used for factory calibration.

  /*
    pinMode(DEBUG_DC, INPUT);
    digitalWrite(DEBUG_DC, LOW);
    pinMode(SS2, INPUT);
    digitalWrite(SS2, LOW);
  */
  String json2[max_jsons];  // TODO - don't use String   // will contain each json
  for (int i = 0; i < max_jsons; i++) {
    json2[i] = "";  // reset all json2 char's to zero (ie reset all protocols)
  }

  {                                                                      // create limited scope for serial_buffer
    char serial_buffer[serial_buffer_size + 1];                          // large buffer for reading in a json protocol from serial port
    Serial_Input_Chars(serial_buffer, "\r\n", 500, serial_buffer_size);  // input the protocol
    if (!check_protocol(serial_buffer)) {                                // sanity check
      // as `received` is not valid json, this trows out the json parser on the other end
      // would be nice if the json payload was escaped
      Serial_Print("{\"error\":\"bad json protocol (braces or CRC)\"              ");
      Serial_Print(serial_buffer);
      Serial_Print("      \"}");
      Serial_Print_CRC();
      Serial_Flush_Output();
      return;
    }

    // break up the protocol into individual jsons
    // TODO improve this - use in place, stretch it in place or copy to new C strings?
    // make json2 an array of char pointers to each protocol
    for (unsigned i = 1; i < strlen(serial_buffer); i++) {  // increments through each char in incoming transmission - if it's open curly, it saves all chars until closed curly.  Any other char is ignored.
      if (serial_buffer[i] == '{') {                        // wait until you see a open curly bracket
        while (serial_buffer[i] != '}') {                   // once you see it, save incoming data to json2 until you see closed curly bracket
          json2[number_of_protocols] += serial_buffer[i];   // add single char to json
          i++;
        }
        json2[number_of_protocols] += serial_buffer[i];  // catch the last closed curly
        number_of_protocols++;
      }
    }  // for
  }    // no more need for the serial input buffer

  //  turn_on_5V();                             // turn on the +5V and analog circuits

#ifdef DEBUGSIMPLE
  Serial_Printf("got %d protocols\n", number_of_protocols);

  // print each json
  for (int i = 0; i < number_of_protocols; i++) {
    Serial_Printf("Incoming JSON %d as received by Teensy : %s\n", i, json2[i].c_str());
  }  // for
#endif

  int batteryPercent = battery_percent(0);  // percent with no load
#ifdef YESBATT
  if (batteryPercent < 10) {
    Serial_Printf("{\"error\":\"Battery critically low at %d %%, unable to collect data.  Charge device via micro USB.\"}", batteryPercent);
    Serial_Print_CRC();
    Serial_Flush_Output();
    return;
  }
#endif

  Serial_Printf("{\"device_name\":\"%s\",\"device_version\":\"%s\",\"device_id\":\"%2.2x:%2.2x:%2.2x:%2.2x\",\"device_battery\":%d,\"device_firmware\":\"%s\"", DEVICE_NAME, DEVICE_VERSION,  // I did this so it would work with chrome app
                (unsigned)eeprom->device_id >> 24,
                ((unsigned)eeprom->device_id & 0xff0000) >> 16,
                ((unsigned)eeprom->device_id & 0xff00) >> 8,
                (unsigned)eeprom->device_id & 0xff, batteryPercent,
                DEVICE_FIRMWARE);

  Serial_Print(",\"sample\":[");

  // discharge sample and hold in case the cap is currently charged (on add on and main board)

  digitalWriteFast(HOLDM, HIGH);
  delay(10);

  // loop through the all measurements to create a measurement group
  for (int y = 0; y < measurements; y++) {  // measurements is initially 1, but gets updated after the json is parsed

    AD_set();  // set both ADC channels

    for (int q = 0; q < number_of_protocols; q++) {  // loop through all of the protocols to create a measurement

      JsonHashTable hashTable;
      JsonParser<MAX_JSON_ELEMENTS> root;
      char json[json2[q].length() + 1];  // we need a writeable C string
      strncpy(json, json2[q].c_str(), json2[q].length());
      json[json2[q].length()] = '\0';         // Add closing character to char*
      json2[q] = "";                          // attempt to release the String memory
      hashTable = root.parseHashTable(json);  // parse it

      if (!hashTable.success()) {  // NOTE: if the incomign JSON is too long (>~5000 bytes) this tends to be where you see failure (no response from device)
        Serial_Print("{\"error\":\"JSON failure with:\"}");
        Serial_Print(json);
        goto abort;
      }

      int protocols = 1;  // starts as 1 but gets updated when the json is parsed
//      int quit = 0;

      for (int u = 0; u < protocols; u++) {  // the number of times to repeat the current protocol
        //uint16_t open_close_start = hashTable.getLong("open_close_start");            // if open_close_start == 1, then the user must open and close the clamp in order to proceed with the measurement (as measured by hall sensor)
        uint16_t show_voltage = hashTable.getLong("show_voltage");  // show the LED voltage (voltage_raw) along with reflectance measurements (data_raw).  This is used for heat calibration using the LED voltage as a proxy for temperature.
        //        show_voltage = 1;
        uint16_t calibration = hashTable.getLong("calibration");          // is this a calibration measurement.  Must be a whole number (0,1,2,3,4...).  Whatever number you pass it, it will return in the data JSON.
        uint16_t get_calibration = hashTable.getLong("get_calibration");  // print typical required calibration values for reflectometer - black, shiney, and blank values for all detectors and lights. 1 = y, 0 = n.
        JsonArray data_type = hashTable.getArray("data_type");            // specify what data type to return.  0 = normalized and heat calibrated data (0 - 10000), 1 = normalized and heat calibrated data w/ blank offset subtracted (0 - 10000), 2 = raw and heat calibrated data (0 - 65535), 3 and above are raw not heat calibrated.  For raw, not heat calibrated and without reference subtracted, also set reference = 1 in protocol (see reference below);
        JsonArray save_eeprom = hashTable.getArray("save");               // save values to the eeprom.
        JsonArray recall_eeprom = hashTable.getArray("recall");           // userdef values to recall
        JsonArray number_samples = hashTable.getArray("number_samples");  // number of samples on the cap during sample + hold phase (default is 40);
        JsonArray reference = hashTable.getArray("reference");            // subtract ambient detector value directly before read value (ie reference) if set to 0.  Default is reference is on (= 0)
                                                                          //        uint16_t dac_lights =     hashTable.getLong("dac_lights");                                // when inputting light intensity values, use DAC instead of calibrated outputs (do not use expr() function on coming API data)
                                                                          //        dac_lights = 1;                                                                         // REFLECTOMETER does not use calibration of lights to absolute standard, so this should be always set turned on.  If/when we clean up this code, we will want to get rid of this entirely.
        uint16_t adc_show = hashTable.getLong("adc_show");                // this tells the MultispeQ to print the ADC values only instead of the normal data_raw (for signal quality debugging)
        uint16_t adc_only[150];                                           // this and first_adc_ref are used to save the first set of ADC averages produced, so it can be optionally displayed instead of data_raw (for signal quality debugging).  USE ONLY WITH REFERENCE == 0 (ie reference is OFF!)
        JsonArray pulses = hashTable.getArray("pulses");                  // the number of measuring pulses, as an array.  For example [50,10,50] means 50 pulses, followed by 10 pulses, follwed by 50 pulses.
        String protocol_id = hashTable.getString("protocol_id");          // used to determine what macro to apply
        String normalized = hashTable.getString("normalized");            // type of normalization to apply, saved as `normalized_` + type (soil, 5mlcuvette, 1mlcuvette, custom)
        int analog_averages = hashTable.getLong("analog_averages");       // DEPRECIATED IN NEWEST HARDWARE 10/14 # of measurements per measurement pulse to be internally averaged (min 1 measurement per 6us pulselengthon) - LEAVE THIS AT 1 for now
        if (analog_averages == 0) {                                       // if averages don't exist, set it to 1 automatically.
          analog_averages = 1;
        }
        averages = hashTable.getLong("averages");  // The number of times to average this protocol.  The spectroscopic and environmental data is averaged in the device and appears as a single measurement.
        if (averages == 0) {                       // if averages don't exist, set it to 1 automatically.
          averages = 1;
        }
        int averages_delay_ms = hashTable.getLong("averages_delay");      // same as above but in ms
        measurements = hashTable.getLong("measurements");                 // number of times to repeat a measurement, which is a set of protocols
        measurements_delay_ms = hashTable.getLong("measurements_delay");  // delay between measurements in milliseconds
        protocols = hashTable.getLong("protocols");                       // delay between protocols within a measurement
        if (protocols == 0) {                                             // if averages don't exist, set it to 1 automatically.
          protocols = 1;
        }
        int protocols_delay_ms = hashTable.getLong("protocols_delay");  // delay between protocols within a measurement in milliseconds
        if (hashTable.getLong("act_background_light") == 0) {           // The Teensy pin # to associate with the background actinic light.  This light continues to be turned on EVEN BETWEEN PROTOCOLS AND MEASUREMENTS.  It is always Teensy pin 13 by default.
          act_background_light = 0;                                     // change to new background actinic light
        } else {
          act_background_light = hashTable.getLong("act_background_light");  // DEPRECIATED as of 6/29/216
        }
        //averaging0 - 1 - 30
        //averaging1 - 1 - 30
        //resolution0 - 2 - 16
        //resolution1 - 2 - 16
        //conversion_speed - 0 - 5
        //sampling_speed - 0 - 5
        /*
                int averaging0 =          hashTable.getLong("averaging");                               // # of ADC internal averages
                if (averaging0 == 0) {                                                                   // if averaging0 don't exist, set it to 10 automatically.
                  averaging0 = 10;
                }
                //int averaging1 = averaging0;
                int resolution0 =         hashTable.getLong("resolution");                               // adc resolution (# of bits)
                if (resolution0 == 0) {                                                                   // if resolution0 don't exist, set it to 16 automatically.
                  resolution0 = 16;
                }
                //int resolution1 = resolution0;
                int conversion_speed =    hashTable.getLong("conversion_speed");                               // ADC speed to convert analog to digital signal (5 fast, 0 slow)
                if (conversion_speed == 0) {                                                                   // if conversion_speed don't exist, set it to 3 automatically.
                  conversion_speed = 3;
                }
                int sampling_speed =      hashTable.getLong("sampling_speed");                               // ADC speed of sampling (5 fast, 0 slow)
                if (sampling_speed == 0) {                                                                   // if sampling_speed don't exist, set it to 3 automatically.
                  sampling_speed = 3;
                }
        */
        //        int tcs_to_act =            hashTable.getLong("tcs_to_act");                               // sets the % of response from the tcs light sensor to act as actinic during the run (values 1 - 100).  If tcs_to_act is not defined (ie == 0), then the act_background_light intensity is set to actintensity1.
        //int offset_off =          hashTable.getLong("offset_off");                               // turn off detector offsets (default == 0 which is on, set == 1 to turn offsets off)

        ///*
        JsonArray pulsedistance = hashTable.getArray("pulse_distance");  // distance between measuring pulses in us.  Minimum 1000 us.
        JsonArray pulsesize = hashTable.getArray("pulse_length");        // pulse width in us.

        JsonArray a_lights = hashTable.getArray("nonpulsed_lights");
        JsonArray a_intensities = hashTable.getArray("nonpulsed_lights_brightness");
        JsonArray m_intensities = hashTable.getArray("pulsed_lights_brightness");

        //        int get_offset =          hashTable.getLong("get_offset");                               // include detector offset information in the output
        // NOTE: it takes about 50us to set a DAC channel via I2C at 2.4Mz.

        JsonArray detectors = hashTable.getArray("detectors");  // the Teensy pin # of the detectors used during those pulses, as an array of array.  For example, if pulses = [5,2] and detectors = [[34,35],[34,35]] .
        JsonArray meas_lights = hashTable.getArray("pulsed_lights");
        //        JsonArray message =       hashTable.getArray("message");                                // sends the user a message to which they must reply <answer>+ to continue
        //*/
        JsonArray environmental = hashTable.getArray("environmental");
        JsonArray environmental_array = hashTable.getArray("environmental_array");
        uint16_t notArray = 0;
        if (hashTable.getArray("environmental_array").getArray(0).getString(0) == 0) {
          notArray = 1;
        }
        /*
          Serial_Print("this is the thing: ");
          Serial_Print(hashTable.getArray("environmental_array").getArray(0).getString(0));
          Serial_Print(",");
          Serial_Print(notArray);
        */

        long size_of_data_raw = 0;
        long total_pulses = 0;

        for (int i = 0; i < pulses.getLength(); i++) {                              // count the number of non zero lights and total pulses
          total_pulses += pulses.getLong(i) * meas_lights.getArray(i).getLength();  // count the total number of pulses
          int non_zero_lights = 0;
          for (int j = 0; j < meas_lights.getArray(i).getLength(); j++) {  // count the total number of non zero pulses
            if (meas_lights.getArray(i).getLong(j) > 0) {
              non_zero_lights++;
            }
          }

          size_of_data_raw += pulses.getLong(i) * non_zero_lights;

        }  // for each pulse

        Serial_Print("{");

        /* DEPRECIATED
                Serial_Print("\"protocol_id\":\"");
                Serial_Print(protocol_id.c_str());
                Serial_Print("\",");

                Serial_Print("\"object_type\":\"");
                Serial_Print(object_type.c_str());
                Serial_Print("\",");
        */

        if (calibration != 0) {
          Serial_Printf("\"calibration\":%d,", calibration);
        }

        // print the reflectometer calibration values if requested.
        if (get_calibration == 1) {
          unsigned i;
          Serial_Print("\"ir_high\": [");
          for (i = 0; i < arraysize(eeprom->ir_high) - 1; i++)
            Serial_Printf("\"%f\",", eeprom->ir_high[i]);
          Serial_Printf("\"%f\"],", eeprom->ir_high[i]);

          Serial_Print("\"ir_low\": [");
          for (i = 0; i < arraysize(eeprom->ir_low) - 1; i++)
            Serial_Printf("\"%f\",", eeprom->ir_low[i]);
          Serial_Printf("\"%f\"],", eeprom->ir_low[i]);

          Serial_Print("\"vis_high\": [");
          for (i = 0; i < arraysize(eeprom->vis_high) - 1; i++)
            Serial_Printf("\"%f\",", eeprom->vis_high[i]);
          Serial_Printf("\"%f\"],", eeprom->vis_high[i]);

          Serial_Print("\"vis_low\": [");
          for (i = 0; i < arraysize(eeprom->vis_low) - 1; i++)
            Serial_Printf("\"%f\",", eeprom->vis_low[i]);
          Serial_Printf("\"%f\"],", eeprom->vis_low[i]);

          Serial_Print("\"ir_high_master\": [");
          for (i = 0; i < arraysize(eeprom->ir_high_master) - 1; i++)
            Serial_Printf("\"%f\",", eeprom->ir_high_master[i]);
          Serial_Printf("\"%f\"],", eeprom->ir_high_master[i]);

          Serial_Print("\"ir_low_master\": [");
          for (i = 0; i < arraysize(eeprom->ir_low_master) - 1; i++)
            Serial_Printf("\"%f\",", eeprom->ir_low_master[i]);
          Serial_Printf("\"%f\"],", eeprom->ir_low_master[i]);

          Serial_Print("\"vis_high_master\": [");
          for (i = 0; i < arraysize(eeprom->vis_high_master) - 1; i++)
            Serial_Printf("\"%f\",", eeprom->vis_high_master[i]);
          Serial_Printf("\"%f\"],", eeprom->vis_high_master[i]);

          Serial_Print("\"vis_low_master\": [");
          for (i = 0; i < arraysize(eeprom->vis_low_master) - 1; i++)
            Serial_Printf("\"%f\",", eeprom->vis_low_master[i]);
          Serial_Printf("\"%f\"],", eeprom->vis_low_master[i]);

          Serial_Print("\"ir_blank\": [");
          for (i = 0; i < arraysize(eeprom->ir_blank) - 1; i++)
            Serial_Printf("\"%f\",", eeprom->ir_blank[i]);
          Serial_Printf("\"%f\"],", eeprom->ir_blank[i]);

          Serial_Print("\"vis_blank\": [");
          for (i = 0; i < arraysize(eeprom->vis_blank) - 1; i++)
            Serial_Printf("\"%f\",", eeprom->vis_blank[i]);
          Serial_Printf("\"%f\"],", eeprom->vis_blank[i]);

          Serial_Print("\"heatcal1\": [");
          for (i = 0; i < arraysize(eeprom->heatcal1) - 1; i++)
            Serial_Printf("\"%f\",", eeprom->heatcal1[i]);
          Serial_Printf("\"%f\"],", eeprom->heatcal1[i]);

          Serial_Print("\"heatcal2\": [");
          for (i = 0; i < arraysize(eeprom->heatcal2) - 1; i++)
            Serial_Printf("\"%f\",", eeprom->heatcal2[i]);
          Serial_Printf("\"%f\"],", eeprom->heatcal2[i]);

          Serial_Print("\"heatcal3\": [");
          for (i = 0; i < arraysize(eeprom->heatcal3) - 1; i++)
            Serial_Printf("\"%f\",", eeprom->heatcal3[i]);
          Serial_Printf("\"%f\"],", eeprom->heatcal3[i]);
          /*
                    Serial_Print("\"heatcal4\": [");
                    for (i = 0; i < arraysize(eeprom->heatcal4) - 1; i++)
                      Serial_Printf("\"%f\",", eeprom->heatcal4[i]);
                    Serial_Printf("\"%f\"],", eeprom->heatcal4[i]);

                    Serial_Print("\"heatcal5\": [");
                    for (i = 0; i < arraysize(eeprom->heatcal5) - 1; i++)
                      Serial_Printf("\"%f\",", eeprom->heatcal5[i]);
                    Serial_Printf("\"%f\"],", eeprom->heatcal5[i]);
          */
        }
        float data_raw_average[size_of_data_raw];     // buffer for ADC output data
        float voltage_raw_average[size_of_data_raw];  // buffer for ADC output data
        for (int i = 0; i < size_of_data_raw; ++i)    // zero it
          data_raw_average[i] = 0;
        for (int i = 0; i < size_of_data_raw; ++i)  // zero it
          voltage_raw_average[i] = 0;

        uint16_t env_counter = 0;
        for (uint16_t i = 0; i < environmental_array.getLength(); i++) {  // identify how many arrays to save for each requested environmental variable, based on the number of outputs per call... so compass yields two arrays ("compass" and "angle")... etc.
          theReadings thisSensor = getReadings(environmental_array.getArray(i).getString(0));
          env_counter = env_counter + thisSensor.numberReadings;  // sum the total number of readings, so we create the right number of arrays.
        }
        //                Serial_Printf("env_counter: %d, size_of_data_raw: %d", env_counter, size_of_data_raw);
        float environmental_array_averages[env_counter][size_of_data_raw];  // buffer for each of the environmentals as arrays in environmental_array_averages
        if (env_counter > 0) {                                              // if there are environmentals during pulse sets, then generate an array to store the outputs in
          for (uint16_t i = 0; i < env_counter; i++) {
            for (uint16_t j = 0; j < size_of_data_raw; j++) {  // initialize the array as zeros
              environmental_array_averages[i][j] = 0;
            }
          }
        }

#ifdef DEBUGSIMPLE
        Serial_Print_Line("");
        Serial_Print("size of data raw:  ");
        Serial_Print_Line(size_of_data_raw);

        Serial_Print_Line("");
        Serial_Print("total number of pulses:  ");
        Serial_Print_Line(total_pulses);

        Serial_Print_Line("");
        Serial_Print("all data in data_raw_average:  ");
        for (int i = 0; i < size_of_data_raw; i++) {
          Serial_Print((unsigned)data_raw_average[i]);
        }

        Serial_Print_Line("");
        Serial_Print("number of pulses:  ");
        Serial_Print_Line(pulses.getLength());

        Serial_Print_Line("");
        Serial_Print("arrays in meas_lights:  ");
        Serial_Print_Line(meas_lights.getLength());

        Serial_Print_Line("");
        Serial_Print("length of meas_lights arrays:  ");
        for (int i = 0; i < meas_lights.getLength(); i++) {
          Serial_Print(meas_lights.getArray(i).getLength());
          Serial_Print(", ");
        }
        Serial_Print_Line("");
#endif
        /*
                if (get_offset == 1) {
                  print_offset(1);
                }
        */

        if (averages > 1) {
          Serial_Print("\"averages\":");
          Serial_Print(averages);
          Serial_Print(",");
        }

        //        print_sensor_calibration(1);                                               // print sensor calibration data

        // clear variables (many not needed)

        //        light_intensity = light_intensity_averaged = 0;
        //        light_intensity_raw = light_intensity_raw_averaged = 0;
        //        r = r_averaged =  g = g_averaged = b = b_averaged = 0;

        //        thickness = thickness_averaged = 0;
        //        thickness_raw = thickness_raw_averaged = 0;

        //        contactless_temp = contactless_temp_averaged = 0;

        /*
                compass = compass_averaged = 0;
                x_compass_raw = 0, y_compass_raw = 0, z_compass_raw = 0;
                x_compass_raw_averaged = 0, y_compass_raw_averaged = 0, z_compass_raw_averaged = 0;

                angle = 0;
                angle_averaged = 0;
                angle_direction = "";
                roll = roll_averaged = 0;
                pitch = pitch_averaged = 0;
                x_tilt = 0, y_tilt = 0, z_tilt = 0;
                x_tilt_averaged = 0, y_tilt_averaged = 0, z_tilt_averaged = 0;
        */
        temperature = humidity = pressure = voc = 0;
        temperature_averaged = humidity_averaged = pressure_averaged = voc_averaged = 0;

        //        temperature2 = humidity2 = pressure2 = 0;
        //        temperature2_averaged = humidity2_averaged = pressure2_averaged = 0;

        //        co2 = co2_averaged = 0;

        detector_read1 = detector_read1_averaged = 0;
        detector_read2 = detector_read2_averaged = 0;
        detector_read3 = detector_read3_averaged = 0;

        analog_read = digital_read = 0;
        analog_read_averaged = digital_read_averaged = 0;

        // if (hashTable.getLong("open_close_start") == 1) {                                     // wait for device to open (read hall sensor), then close before proceeding with protocol
        //   start_on_open_close();
        // }

        // if (hashTable.getLong("pin_high_start") != 0) {                                     // wait for device to open (read hall sensor), then close before proceeding with protocol
        //   start_on_pin_high(hashTable.getLong("pin_high_start"));
        // }

        // perform the protocol averages times
        for (int x = 0; x < averages; x++) {  // Repeat the protocol this many times

          if (Serial_Available() && Serial_Input_Long("+", 1) == -1) {  // test for abort command
            q = number_of_protocols - 1;
            y = measurements - 1;
            u = protocols;
            x = averages;
          }

          int background_on = 0;
          long data_count = 0;
//          int message_flag = 0;         // flags to indicate if an alert, prompt, or confirm have been called at least once (to print the object name to data JSON)
          unsigned _pulsedistance = 0;  // initialize variables for pulsesize and pulsedistance (as well as the previous cycle's pulsesize and pulsedistance).  We define these only once per cycle so we're not constantly calling the JSON (which is slow)
          unsigned _pulsedistance_prev = 0;
          //          uint16_t _reference_flag = 0;                                                           // used to note if this is the first measurement
          //          float _reference_start = 0;                                                            // reference value at data point 0 - initial value for normalizing the reference (normalized based on the values from main and reference in the first point in the trace)
          //          float _main_start = 0;                                                               // main detector (sample) value at data point 0 - initial value for normalizing the reference (normalized based on the values from main and reference in the first point in the trace)
          uint16_t _number_samples = 0;    // create the adc sampling rate number
          double voltage_accumulator = 0;  // create an average voltage value across the measurement to reduce noise.  This accumulates the voltage throughout the pulse set.

          if (notArray == 1) {
            environmentals(environmental, averages, x, 0);  // if the protocol requests additional environmental measurements, runs them as well.
          }

          for (int z = 0; z < total_pulses; z++) {  // cycle through all of the pulses from all cycles
//            int first_flag = 0;                     // flag to note the first pulse of a cycle

            if (pulse == 0) {                                             // if it's the first pulse of a cycle, we need to set up the new set of lights and intensities...
              meas_array_size = meas_lights.getArray(cycle).getLength();  // get the number of measurement/detector subsets in the new cycle
              voltage_accumulator = 0;

              if (PULSERDEBUG) {
                Serial_Printf("\n _number_samples: %d \n", _number_samples);
              }  // PULSERDEBUG

              for (unsigned i = 0; i < NUM_LEDS; i++) {  // save the list of act lights in the previous pulse set to turn off later
                _a_lights_prev[i] = _a_lights[i];
                if (PULSERDEBUG) {
                  Serial_Printf("\n all a_lights_prev: %d\n", _a_lights_prev[i]);
                }  // PULSERDEBUG
              }

              for (unsigned i = 0; i < NUM_LEDS; i++) {              // save the current list of act lights, determine if they should be on, and determine their intensity
                _a_lights[i] = a_lights.getArray(cycle).getLong(i);  // save which light should be turned on/off
                String intensity_string = a_intensities.getArray(cycle).getString(i);
                _a_intensities[i] = expr(intensity_string.c_str());  // evaluate as an expression

                if (PULSERDEBUG) {
                  Serial_Printf("\n all a_lights, intensities: %d,%d,|%s|,%f,%f,%f\n", _a_lights[i], _a_intensities[i], intensity_string.c_str(), expr(intensity_string.c_str()), light_intensity, light_intensity_averaged);
                }  // PULSERDEBUG
              }

              if (cycle != 0) {
                _pulsedistance_prev = _pulsedistance;
              }
              String distanceString = pulsedistance.getString(cycle);  // initialize variables for pulsesize and pulsedistance (as well as the previous cycle's pulsesize and pulsedistance).  We define these only once per cycle so we're not constantly calling the JSON (which is slow)
              _pulsedistance = expr(distanceString.c_str());           // initialize variables for pulsesize and pulsedistance (as well as the previous cycle's pulsesize and pulsedistance).  We define these only once per cycle so we're not constantly calling the JSON (which is slow)
//              first_flag = 1;                                          // flip flag indicating that it's the 0th pulse and a new cycle
              if (cycle == 0) {                                        // if it's the beginning of a measurement (cycle == 0 and pulse == 0), then...
                //                digitalWriteFast(act_background_light_prev, LOW);                                               // turn off actinic background light and...
                startTimers(_pulsedistance);                                       // Use one ISR to turn on and off the measuring lights.
              } else if (cycle != 0 && (_pulsedistance != _pulsedistance_prev)) {  // if it's not the 0th cycle and the last pulsesize or pulsedistance was different than the current one, then stop the old timers and set new ones.   If they were the same, avoid resetting the timers by skipping this part.
                startTimers(_pulsedistance);                                       // restart the measurement light timer
              }

            }  // if pulse == 0


            _number_samples = number_samples.getLong(cycle);  // set the _number_samples for this cycle
            //            assert(_number_samples >= 0 && _number_samples < 500);

            _meas_light = meas_lights.getArray(cycle).getLong(meas_number % meas_array_size);                  // move to next measurement light
            String intensity_string = m_intensities.getArray(cycle).getString(meas_number % meas_array_size);  // evaluate inputted intensity to see if it was an expression
            uint16_t _m_intensity = expr(intensity_string.c_str());
            //            assert(_m_intensity >= 0 && _m_intensity <= 4095);

            uint16_t detector = detectors.getArray(cycle).getLong(meas_number % meas_array_size);  // move to next detector
            //            assert(detector >= 1 && detector <= 10);

            uint16_t _reference = reference.getArray(cycle).getLong(meas_number % meas_array_size);

            String sizeString = pulsesize.getArray(cycle).getString(meas_number % meas_array_size);  // set the pulse size for the next light
            _pulsesize = expr(sizeString.c_str());

            if (_number_samples == 0) {  // if _number_samples wasn't set or == 0, set it automatically to 19 (default)
              _number_samples = 9;
            }
            if (PULSERDEBUG) {
              Serial_Printf("measurement light, intensity, detector, reference:  %d, %d, %d, %d\n", _meas_light, _m_intensity, detector, _reference);
              Serial_Printf("pulsedistance = %d, pulsesize = %d, cycle = %d, measurement number = %d, measurement array size = %d,total pulses = %d\n", (int)_pulsedistance, (int)_pulsesize, (int)cycle, (int)meas_number, (int)meas_array_size, (int)total_pulses);
            }  // PULSERDEBUG

            if (pulse < meas_array_size) {  // if it's the first pulse of a cycle, then change act 1,2,3,4 values as per array's set at beginning of the file

              if (pulse == 0) {
                //                String _message_type = message.getArray(cycle).getString(0);                                // get what type of message it is
                // if ((_message_type != "" || quit == -1) && x == 0) {                                         // if there are some messages or the user has entered -1 to quit AND it's the first repeat of an average (so it doesn't ask these question on every average), then print object name...
                //   if (message_flag == 0) {                                                                 // if this is the first time the message has been printed, then print object name
                //     Serial_Print("\"message\":[");
                //     message_flag = 1;
                //   }
                //   Serial_Print("[\"");
                //   Serial_Print(_message_type.c_str());                                                               // print message
                //   Serial_Print("\",");
                //   Serial_Print("\"");
                //   Serial_Print(message.getArray(cycle).getString(1));
                //   Serial_Print("\",");
                //   if (_message_type == "0") {
                //     Serial_Print("\"\"]");
                //   }
                //   else if (_message_type == "alert") {                                                    // wait for user response to alert
                //     stopTimers();                                                                         // pause the timers (so the measuring light doesn't stay on
                //     while (1) {
                //       long response = Serial_Input_Long("+", 0);
                //       Serial_Print("\"ok\"]");
                //       break;
                //     }
                //     startTimers(_pulsedistance);                                                // restart the measurement light timer
                //   }
                //   else if (_message_type == "confirm") {                                                  // wait for user's confirmation message.  If enters '1' then skip to end.
                //     stopTimers();                                                                         // pause the timers (so the measuring light doesn't stay on
                //     while (1) {
                //       long response = Serial_Input_Long("+", 0);
                //       if (response == 1) {
                //         Serial_Print("\"cancel\"]]");                                                     // set all loops (protocols, measurements, averages, etc.) to the last loop value so it exits gracefully
                //         q = number_of_protocols;
                //         y = measurements - 1;
                //         u = protocols - 1;
                //         x = averages;
                //         z = total_pulses;
                //         break;
                //       }
                //       else if (response == -1) {
                //         Serial_Print("\"ok\"]");
                //         break;
                //       }
                //     }
                //     startTimers(_pulsedistance);                                                // restart the measurement light timer
                //   }
                //   else if (_message_type == "prompt") {                                                    // wait for user to input information, followed by +
                //     stopTimers();                                                                         // pause the timers (so the measuring light doesn't stay on
                //     char response[150];
                //     Serial_Input_Chars(response, "+", 0, sizeof(response));
                //     Serial_Print("\"");
                //     Serial_Print(response);
                //     Serial_Print("\"]");
                //     startTimers(_pulsedistance);                                                // restart the measurement light timer
                //   }
                //   if (cycle != pulses.getLength() - 1) {                                                  // if it's not the last cycle, then add comma
                //     Serial_Print(",");
                //   }
                //   else {                                                                                 // if it is the last cycle, then close out the array
                //     Serial_Print("],");
                //   }
                // }
                delayMicroseconds(200);
              }

              // calculate_intensity(_meas_light, tcs_to_act, cycle, _light_intensity);                   // in addition, calculate the intensity of the current measuring light

              //            Serial_Printf("_meas_light = %d, par_to_dac = %d, _m_intensity = %d, dac_lights = %d\n",_meas_light,par_to_dac(_m_intensity, _meas_light),_m_intensity,dac_lights);

              //  if (!dac_lights)                                                     // evaluate as an expression...
              //    DAC_set(_meas_light, par_to_dac(_m_intensity, _meas_light));       // set the DAC, make sure to convert PAR intensity to DAC value
              //  else                                                                 // otherwise evaluate directly as a number to enter into the DAC
              DAC_set(_meas_light, _m_intensity);  // set the DAC, make sure to convert PAR intensity to DAC value
            }                                      // if (pulse < meas_array_size)

            if (Serial_Available() && Serial_Input_Long("+", 1) == -1) {  // exit protocol completely if user enters -1+
              q = number_of_protocols;
              y = measurements - 1;
              u = protocols - 1;
              x = averages;
              z = total_pulses;
            }

            int32_t sample_adc[_number_samples];  // initialize the variables to hold the main and reference detector data
            int32_t sample_adc_ref[_number_samples];
            //            uint16_t startTimer;                                                                            // to measure the actual time it takes to perform the ADC reads on the sample (for debugging)
            //            uint16_t endTimer;

            pulse_done = 0;        // clear volatile ISR done flag
            while (!pulse_done) {  // wait for LED pulse complete (in ISR)
              //if (abort_cmd())
              //  goto abort;  // or just reboot?
              //sleep_cpu();     // save power - removed, causes race condition
            }

            //              AD7689_read_array(sample_adc, _number_samples);                                              // just reads the detector defined by the user
            AD_read_array(sample_adc, detector, _number_samples);  // updated ADC read from teensy ADC
            _led_voltage = 0;
            for (uint16_t i = 0; i < 15; i++) {  // takes ~75 microseconds - significantly reduces noise
              _led_voltage += analogRead(A7);
            }
            _led_voltage = _led_voltage / 15;
            if (pulse < 3) {  // the first few pulses can sometimes be spikes, so do not add them to the voltage_accumulator, they'll just shift the overall average up or down by too much.
              voltage_accumulator = _led_voltage;
            } else {
              voltage_accumulator += _led_voltage;
            }
#ifdef TIA
            digitalWriteFast(PULSE_OFF, LOW);                          // turn off measuring light - leave in for TIA (transimpedence amp)
            delayMicroseconds(1);                                      // wait a very short time to let the off settle (probably overkill, consider test / remove)
            AD_read_array(sample_adc_ref, detector, _number_samples);  // Now read background reference, to be subtracted from the signal
#endif
            interrupts();  // re-enable interrupts (left off after LED ISR)

            //              Serial_Printf("accumulate: %.2f, voltage: %.2f\n", voltage_accumulator, (float) _led_voltage);

            digitalWriteFast(HOLDM, HIGH);  // discharge integrators

            if (env_counter > 0) {  // check to see if there are any objects in environmental array (this saves some time as calling .getLength on environmental_array can take some time)
              environmentals(environmental_array, averages, x, 1);
              uint16_t counter1 = 0;
              for (uint16_t i = 0; i < environmental_array.getLength(); i++) {
                theReadings thisSensor = getReadings(environmental_array.getArray(i).getString(0));
                // *** HERE ***


                environmental_array_averages[counter1][z] = expr(thisSensor.reading1);
                //              Serial_Printf("counter reading 1:%d, array: %s, value: %f\n", counter1, thisSensor.reading1, expr(thisSensor.reading1));
                counter1++;
                if (thisSensor.numberReadings >= 2) {  // so if the number of readings is 1 or fewer, do this, next if statement is for 2 or fewer, etc. etc.
                  environmental_array_averages[counter1][z] = expr(thisSensor.reading2);
                  //                  Serial_Printf("counter reading 2:%d, array: %s, value: %f\n", counter1, thisSensor.reading2, expr(thisSensor.reading2));
                  counter1++;
                }
                if (thisSensor.numberReadings >= 3) {  // so if the number of readings is 1 or fewer, do this, next if statement is for 2 or fewer, etc. etc.
                  environmental_array_averages[counter1][z] = expr(thisSensor.reading3);
                  //                  Serial_Printf("counter reading 3:%d, array: %s, value: %f\n", counter1, thisSensor.reading3, expr(thisSensor.reading3));
                  counter1++;
                }
                if (thisSensor.numberReadings >= 4) {  // so if the number of readings is 1 or fewer, do this, next if statement is for 2 or fewer, etc. etc.
                  environmental_array_averages[counter1][z] = expr(thisSensor.reading4);
                  //                  Serial_Printf("counter reading 4:%d, array: %s, value: %f\n", counter1, thisSensor.reading4, expr(thisSensor.reading4));
                  counter1++;
                }
                if (thisSensor.numberReadings >= 5) {  // so if the number of readings is 1 or fewer, do this, next if statement is for 2 or fewer, etc. etc.
                  environmental_array_averages[counter1][z] = expr(thisSensor.reading5);
                  //                  Serial_Printf("counter reading 5:%d, array: %s, value: %f\n", counter1, thisSensor.reading5, expr(thisSensor.reading5));
                  counter1++;
                }
              }
            }
            //              Serial_Print("I stopped here");

            if (adc_show == 1) {                                                      // save the individual ADC measurements separately if adc_show is set to 1 (to be printed to the output later instead of data_raw)
              for (unsigned i = 0; i < sizeof(sample_adc) / ( sizeof(uint16_t) ); i++) {  //
                adc_only[i] = sample_adc[i];
              }
            }
            data = mediani(sample_adc, _number_samples);  // using median - 25% improvements over using mean to determine this value
            /*
              Serial_Print_Line("");
              Serial_Print("data here: ");
              Serial_Print_Line(data);
              Serial_Print("data_type: ");
              Serial_Print_Line(data_type.getLong(cycle));
            */
            // Heat calibration must be applied to both raw signals (measurement and reference) prior to them being subtracted and prior to normalization. Can be used with poly fits, but currently fits best with simple linear fit.
            // Put the current temperature though the model ax4 + bx3 + cx2 + dx1 + e, we get the % shift in the current detector value, where 1 is no shift, .2 is 20% of current value, 1.5 is 150% of current value, etc.
            // Good test routine for this -->
            if (data_type.getLong(cycle) == 0 || data_type.getLong(cycle) == 1 || data_type.getLong(cycle) == 2) {  // only apply heat calibration if it's data_type 0, 1, or 2
              double runningVoltage;
              if (pulse < 3) {
                runningVoltage = voltage_accumulator;
              } else {
                runningVoltage = voltage_accumulator / (pulse - 1);
              }
              //              Serial_Print_Line(runningVoltage,2);
              //              double runningVoltage = _led_voltage;
              double a = 0;  //temperature * temperature * temperature * temperature * eeprom->heatcal5[_meas_light] / 1000000000;
              double b = 0;  //temperature * temperature * temperature * eeprom->heatcal4[_meas_light] / 1000000;
              double c = (runningVoltage * runningVoltage * eeprom->heatcal3[_meas_light]) / 1000;
              double d = runningVoltage * eeprom->heatcal2[_meas_light];
              double e = eeprom->heatcal1[_meas_light];
              double heat_mod = 1 / (a + b + c + d + e);  // have to divide it by 1 so we show what we need to multiply the raw result by to get the heat calibrated result.
              //              Serial_Print_Line("Heat Mod");
              //              Serial_Print_Line(heat_mod,5);
              //               Serial_Print_Line("Equation: heatModifier = 1 / ( temp^4*heatcal5 + temp^3*heatcal4 + temp^2*heatcal3 + temp*heatcal2 + heatcal1 ).  Note heatcal5/4/3 need to be divided by numbers before calculating result.");
              //               Serial_Printf("LED: %d, Heatmod: %.10f, dataBefore: %d, voltage: %d, heatcal1: %.10f, heatcal2: %.10f, heatcal3: %.10f / 1,000", _meas_light, heat_mod, data, runningVoltage, eeprom->heatcal1[_meas_light], eeprom->heatcal2[_meas_light], eeprom->heatcal3[_meas_light]);
              data = data * heat_mod;
              //              Serial_Printf(", dataAfter: %d\n", data);
            }
            if (_reference == 0) {                                  // if also using reference, then ...
              data_ref = mediani(sample_adc_ref, _number_samples);  // generate median of reference values
            }
            data = data - data_ref;  // subtract ambient reference value after light was turned off from value when light was turned on (accounts for non-pulsing ambient light)
                                     /*
              Serial_Print_Line("");
              Serial_Print("data here: ");
              Serial_Print_Line(data);
              data = data - data_ref; // subtract ambient reference value after light was turned off from value when light was turned on (accounts for non-pulsing ambient light)
              Serial_Print_Line("");
              Serial_Print("data here: ");
              Serial_Print_Line(data);
            */

// #ifdef PULSERDEBUG
//             //Serial_Printf("median + first value :%d,%d", data, sample_adc[5]);
//             //Serial_Printf("median + first value reference :%d,%d", data_ref, sample_adc_ref[5]);
//           }  // PULSERDEBUG

//           if (first_flag == 1) {  // if this is the 0th pulse and a therefore new cycle
//             first_flag = 0;       // reset flag
// #endif

#ifdef DEBUGSIMPLE
            Serial_Print("data count, size of raw data                                   ");
            Serial_Print((int)data_count);
            Serial_Print(",");
            Serial_Print_Line(size_of_data_raw);
#endif
            if (_meas_light != 0) {     // save the data, so long as the measurement light is not equal to zero.
              float check_data = data;  // make sure that the 32 bit value is within range before we stick it into the 16 bit value.
              if (check_data < 0) {
                check_data = 0;
              } else if (check_data > 65535) {
                check_data = 65535;
              }
              check_data = check_data * 10000;                                       // make the numbers larger so when we're doing math on them later we're not getting into very small numbers...
              if (data_type.getLong(cycle) == 0 || data_type.getLong(cycle) == 1) {  // convert data to relative value (0 - 100), raw value (0 - 65535), or relative value with blank offset (0 - 100)
                if (detector == 0) {
                  double zeroOffset = check_data - eeprom->ir_low[_meas_light];
                  double range = eeprom->ir_high[_meas_light] - eeprom->ir_low[_meas_light];
                  double check_data_ratio = zeroOffset / range;
                  check_data = check_data_ratio * eeprom->ir_high_master[_meas_light];  // 0 - 10,000 is the range we're calibrating to
                  //                    Serial_Printf("\"normalized\" = %.6f,", check_data);
                  if (normalized == "soil") {
                      check_data = check_data + ( abs(check_data) * ( eeprom->normalized_soil[_meas_light] - 1 ) );
                  } else if (normalized == "5mlcuvette") {
                      check_data = check_data + ( abs(check_data) * ( eeprom->normalized_5mlcuvette[_meas_light] - 1 ) );
                  } else if (normalized == "1mlcuvette") {
                      check_data = check_data + ( abs(check_data) * ( eeprom->normalized_1mlcuvette[_meas_light] - 1 ) );
                  } else if (normalized == "custom") {
                      check_data = check_data + ( abs(check_data) * ( eeprom->normalized_custom[_meas_light] - 1 ) );
                  }
                  //                    Serial_Printf("\"normalized_by_type\" = %.6f,", check_data);
                  if (data_type.getLong(cycle) == 1) {  // subtract the blank
                    check_data = check_data - eeprom->ir_blank[_meas_light];
                    //                      Serial_Printf("\"normalized_w_blank\" = %.6f,", check_data);
                  }
                }
                if (detector == 1) {

                  double zeroOffset = check_data - eeprom->vis_low[_meas_light];
                  double range = eeprom->vis_high[_meas_light] - eeprom->vis_low[_meas_light];
                  double check_data_ratio = zeroOffset / range;
                  check_data = check_data_ratio * eeprom->vis_high_master[_meas_light];  // 0 - 10,000 is the range we're calibrating to
                  //                    Serial_Printf("\"normalized\" = %.6f,", check_data);
                  if (normalized == "soil") {
                      check_data = check_data + ( abs(check_data) * ( eeprom->normalized_soil[_meas_light] - 1 ) );
                  } else if (normalized == "5mlcuvette") {
                      check_data = check_data + ( abs(check_data) * ( eeprom->normalized_5mlcuvette[_meas_light] - 1 ) );
                  } else if (normalized == "1mlcuvette") {
                      check_data = check_data + ( abs(check_data) * ( eeprom->normalized_1mlcuvette[_meas_light] - 1 ) );
                  } else if (normalized == "custom") {
                      check_data = check_data + ( abs(check_data) * ( eeprom->normalized_custom[_meas_light] - 1 ) );
                  }
                  //                    Serial_Printf("\"normalized_by_type\" = %.6f,", check_data);
                  if (data_type.getLong(cycle) == 1) {  // subtract the blank
                    check_data = check_data - eeprom->vis_blank[_meas_light];
                    //                      Serial_Printf("\"normalized_w_blank\" = %.6f,", check_data);
                  }
                  /*
                      Serial_Printf("\"normalized\" = %.6f,", check_data);
                      Serial_Printf("\"meas\" = %d,", _meas_light);
                      Serial_Printf("\"detector\" = %d,", detector);
                      Serial_Printf("\"data_type\" = %d,", data_type.getLong(cycle));
                    */
                }
              } else {  // if data_type is anything but 0 or 1, just output the raw values.
              }
              data_raw_average[data_count] += check_data;  // putting the data into the saved array for delivery to USB or bluetooth.
              if (show_voltage == 1) {
                voltage_raw_average[data_count] += _led_voltage;  // putting the data into the saved array for delivery to USB or bluetooth.
              }
              data_count++;
            }

            pulse++;  // progress the pulse counter and measurement number counter

#ifdef DEBUGSIMPLE
            Serial_Print("data point average, current data                               ");
            Serial_Print((int)data_raw_average[meas_number]);
            Serial_Print("!");
            Serial_Print_Line(data);
#endif
            meas_number++;  // progress measurement number counters

            if (pulse == pulses.getLong(cycle) * meas_lights.getArray(cycle).getLength()) {  // if it's the last pulse of a cycle...
              pulse = 0;                                                                     // reset pulse counter
              cycle++;                                                                       // ...move to next cycle
            }

          }  // for pulses z

          background_on = 0;
          /*
                    background_on = calculate_intensity_background(act_background_light, tcs_to_act, cycle, _light_intensity, act_background_light_intensity); // figure out background light intensity and state
          */

          for (unsigned i = 0; i < NUM_LEDS; i++) {
            if (_a_lights[i] != act_background_light) {  // turn off all lights unless they are the actinic background light
              digitalWriteFast(LED_to_pin[_a_lights[i]], LOW);
            }
          }

          if (background_on == 1) {
            DAC_change();                                  // initiate actinic lights which were set above
            digitalWriteFast(act_background_light, HIGH);  // turn on actinic background light in case it was off previously.
          } else {
            digitalWriteFast(act_background_light, LOW);  // turn on actinic background light in case it was off previously.
          }

          stopTimers();
          cycle = 0;  // ...and reset counters
          pulse = 0;
          meas_number = 0;

          /*
            options for relative humidity, temperature, contactless temperature. light_intensity,co2
            0 - take before spectroscopy measurements
            1 - take after spectroscopy measurements
          */

          if (x + 1 < averages) {  //  to next average, unless it's the end of the very last run
            if (averages_delay_ms > 0) {
              Serial_Input_Long("+", averages_delay_ms);
            }
          }

        }  // for each protocol repeat x for averages

        /*
           Recall and save values to the eeprom
        */

        recall_save(recall_eeprom, save_eeprom);  // Recall and save values to the eeprom.

        // *** IT'S PRINTING FROM HERE ***
        uint16_t counter1 = 0;
        //        Serial_Print("now I'm printing, can I do environmental array averages from here?");
        //        Serial_Print(environmental_array.getLength());
        //        Serial_Print(environmental_array[0].getLength());

        for (uint16_t i = 0; i < environmental_array.getLength(); i++) {  // print the environmental_array data
          theReadings thisSensor = getReadings(environmental_array.getArray(i).getString(0));
          for (uint16_t g = 0; g < thisSensor.numberReadings; g++) {  // print the appropriate sensor value
            switch (g) {
              case 0:  // so if the number of readings is 1 or fewer, do this, next if statement is for 2 or fewer, etc. etc.
                Serial_Printf("\"%s\":[", thisSensor.reading1);
                break;
              case 1:  // so if the number of readings is 1 or fewer, do this, next if statement is for 2 or fewer, etc. etc.
                Serial_Printf("\"%s\":[", thisSensor.reading2);
                break;
              case 2:  // so if the number of readings is 1 or fewer, do this, next if statement is for 2 or fewer, etc. etc.
                Serial_Printf("\"%s\":[", thisSensor.reading3);
                break;
              case 3:  // so if the number of readings is 1 or fewer, do this, next if statement is for 2 or fewer, etc. etc.
                Serial_Printf("\"%s\":[", thisSensor.reading4);
                break;
              case 4:  // so if the number of readings is 1 or fewer, do this, next if statement is for 2 or fewer, etc. etc.
                Serial_Printf("\"%s\":[", thisSensor.reading5);
                break;
            }

            // *** here it's adding the actual saved sensor value ***
            for (uint16_t j = 0; j < size_of_data_raw; j++) {
              if (j != size_of_data_raw - 1) {
                Serial_Printf("\"%f\",", environmental_array_averages[counter1][j]);
              } else {
                Serial_Printf("\"%f\"],", environmental_array_averages[counter1][j]);
              }
            }
            counter1++;
          }
        }

        // print the data

          Serial_Print("\"data_raw\":[");
          if (adc_show == 0) {                            // normal condition - show data_raw as per usual
            for (int i = 0; i < size_of_data_raw; i++) {  // print data_raw, divided by the number of averages
              if (data_raw_average[i] > 200) {            // keep the number of significant figures low if the number is large
                Serial_Print(data_raw_average[i] / averages, 0);
              } else {
                Serial_Print(data_raw_average[i] / averages, 4);
              }
              // if average = 1, then it might be better to print data as it is collected
              if (i != size_of_data_raw - 1) {
                Serial_Print(",");
              }          // if
            }            // for
            delay(250);  // if data_raw and voltage_raw are large, this delay may help reduce packet loss in long measurements.
            if (show_voltage == 1) {
              Serial_Print("],\"voltage_raw\":[");
              for (int i = 0; i < size_of_data_raw; i++) {           // print data_raw, divided by the number of averages
                Serial_Print(voltage_raw_average[i] / averages, 0);  // keep significant figures low, voltage are always large (10k+)
                // if average = 1, then it might be better to print data as it is collected
                if (i != size_of_data_raw - 1) {
                  Serial_Print(",");
                }  // if
              }
              delay(250);  // if data_raw and voltage_raw are large, this delay may help reduce packet loss in long measurements.
            }
          } else {  // if adc_show == 1, show first individual adc's only - do not show normal data_raw (used for signal debugging only)
            for (int i = 0; i < number_samples.getLong(0); i++) {
              Serial_Print(adc_only[i]);
              if (i != number_samples.getLong(0) - 1) {
                Serial_Print(",");
              }
            }  // for
          }

          Serial_Print("]}");


#ifdef DEBUGSIMPLE
        Serial_Print("# of protocols repeats, current protocol repeat, number of total protocols, current protocol      ");
        Serial_Print(protocols);
        Serial_Print(",");
        Serial_Print(u);
        Serial_Print(",");
        Serial_Print(number_of_protocols);
        Serial_Print(",");
        Serial_Print_Line(q);
#endif

        if (q < number_of_protocols - 1 || u < protocols - 1) {  // if it's not the last protocol in the measurement and it's not the last repeat of the current protocol, add a comma
          Serial_Print(",");
          if (protocols_delay_ms > 0) {
            Serial_Input_Long("+", protocols_delay_ms);
          }
        } else if (q == number_of_protocols - 1 && u == protocols - 1) {  // if it is the last protocol, then close out the data json
          Serial_Print("]");
        }

        averages = 1;           // number of times to repeat the entire run
        averages_delay_ms = 0;  // seconds wait time between averages
        analog_averages = 1;    // # of measurements per pulse to be averaged (min 1 measurement per 6us pulselengthon)
        for (unsigned i = 0; i < NUM_LEDS; i++) {
          _a_lights[i] = 0;
        }

//        act_background_light_prev = act_background_light;  // set current background as previous background for next protocol

      }  // for each protocol repeat u

    }  // for each protocol q

    // [{      "environmental":[["light_intensity",0]],"pulses": [100,100,100],"a_lights": [[2],[2],[2]],"a_intensities": [["light_intensity_averaged"],[1000],["light_intensity_averaged"]],"pulsedistance": [10000,10000,10000],"m_intensities": [[500],[500],[500]],"pulsesize": [60,60,60],"detectors": [[1],[1],[1]],"meas_lights": [[3],[3],[3]],"averages": 1}]

    Serial_Flush_Input();
    if (y < measurements - 1) {  // if not last measurement
      Serial_Print(",");         // add commas between measurements
      if (measurements_delay_ms > 0) {
        Serial_Input_Long("+", measurements_delay_ms);
      }
    }  // if

  }  // for each measurement y

abort:

  //  Serial_Print("]}");                // terminate output json
  Serial_Print("}");   // terminate output json
  Serial_Print_CRC();  // TODO put this back in one android app is fixed
  Serial_Flush_Output();

  act_background_light = 0;  // ??

  // turn off all lights (just in case)
  for (unsigned i = 1; i <= NUM_LEDS; i++) {
    digitalWriteFast(LED_to_pin[i], LOW);
  }

  return;

}  // do_protocol()

//  routines for LED pulsing


static void pulse3() {  // ISR to turn on/off LED pulse - also controls integration switch

  if (pulse_done)  // skip this pulse if not ready yet
    return;

  const unsigned STABILIZE = 10;  // this delay gives the LED current controller op amp the time needed to stabilize
//  register int pin = LED_to_pin[_meas_light];
  register int pulse_size = _pulsesize;

  //  DAC_set(_meas_light, 4095);                             // set LED intensity
  //  digitalWriteFast(pin, LOW);         // turn on LED multiplexor

  noInterrupts();
  digitalWriteFast(PULSE_OFF, HIGH);  // turn on measuring light
  delayMicroseconds(STABILIZE);       // this delay gives the LED current controller op amp the time needed to turn
  //  for (uint16_t i = 0; i < 1; i++) {
  //    _led_voltage += analogRead(A7);
  //  }
  // the light on completely + stabilize.
  // Very low intensity measuring pulses may require an even longer delay here.
  digitalWriteFast(HOLDM, LOW);   // turn off sample and hold discharge
  delayMicroseconds(pulse_size);  // pulse width
#ifndef TIA
  digitalWriteFast(PULSE_OFF, LOW);  // turn off measuring light - comment out for TIA (transimpedence amp)
#endif
  pulse_done = 1;  // indicate that we are done
  // NOTE:  interrupts are left off and must be re-enabled
}

// schedule the turn on and off of the LED(s) via a single ISR


static IntervalTimer timer0;

inline static void startTimers(unsigned _pulsedistance) {
  timer0.begin(pulse3, _pulsedistance);  // schedule pulses
}

inline static void stopTimers() {
  timer0.end();  // if it's the last cycle and last pulse, then... stop the timers
}

// write userdef values to eeprom
// example json for save: [{"save":[[1,3.43],[2,5545]]}]  for userdef[1] = 3.43 and userdef[2] = 5545
// read userdef and other values from eeprom
// example json for read: [{"recall":["light_slope_all","userdef[1]"]}]

static void recall_save(JsonArray _recall_eeprom, JsonArray _save_eeprom) {
  int number_saves = _save_eeprom.getLength();      // define these explicitly to make it easier to understand the logic
  int number_recalls = _recall_eeprom.getLength();  // define these explicitly to make it easier to understand the logic

  for (int i = 0; i < number_saves; i++) {  // do any saves
    long location = _save_eeprom.getArray(i).getLong(0);
    //    double value_to_save = _save_eeprom.getArray( i).getDouble(1);
    String value_to_save = _save_eeprom.getArray(i).getString(1);
    if (location >= 0 && location <= (long)NUM_USERDEFS)
      store(userdef[location], expr(value_to_save.c_str()));  // save new value in the defined eeprom location
  }

  if (number_recalls > 0) {        // if the user is recalling any saved eeprom values then...
    Serial_Print("\"recall\":{");  // then print the eeprom location number and the value located there

    for (int i = 0; i < number_recalls; i++) {

      String recall_string = _recall_eeprom.getString(i);

      Serial_Printf("\"%s\":%f", recall_string.c_str(), expr(recall_string.c_str()));

      if (i != number_recalls - 1) {
        Serial_Print(",");
      } else {
        Serial_Print("},");
      }
    }  // for
  }    // if
}  // recall_save()

// return true if a Ctrl-A character has been typed
int abort_cmd() {
  return 0;  // TODO
}

/*
   The structure of the sensor calls follows these general rules:
   1) user enters "environmental":[[...],[...]] structure into API, where ... is the call ("light_intensity" or "thickness" for example), and followed by a 0 or 1 (0 if it's before the main spec measurement, or 1 if it's after)
      An example - "environmental":[["tilt",1],["light_intensity",0]] would call tilt after the measurement, and light intensity before the measurement.
   2) Sensor data gets called in 3 ways: either inside the measurement as part of an expression (maybe "a_intensities":[light_intensity/2]), after a measurement or set of averaged measurements.
      In addition, there are often raw and calibrated versions of sensors, like raw tcs value versus the PAR value, or raw hall sensor versus calibrated thickness.
      As a result, for most sensors there is the base version (like x_tilt) which is available in that measurment, an averaged version (like x_tilt_averaged) which is outputted after averaging, and raw versions of each of those (x_tilt and x_tilt_averaged)
*/

//get_temperature_humidity_pressure_voc

void get_temperature_humidity_pressure_voc(int _averages) {  // read temperature, relative humidity, and pressure BME680 module

  bme.begin(0x76);

  temperature = bme.readTemperature();  // temperature in C
  humidity = bme.readHumidity();        // humidity in %
  pressure = bme.readPressure() / 100;  // pressure in millibar
  voc = (float)bme.readGas() / 1000;    // gas resitsance in KOhm

  temperature_averaged += temperature / _averages;  // same as above, but averaged if function requests averaging
  humidity_averaged += humidity / _averages;
  pressure_averaged += pressure / _averages;
  voc_averaged += voc / _averages;

  //  sensorValues thisSensor = {temperature, humidity, pressure, voc};
}

/*
  void get_temperature_humidity_pressure2 (int _averages) {    // read temperature, relative humidity, and pressure BME280 module

  temperature2 = bme2.readTemperature();
  humidity2 = bme2.readHumidity();
  pressure2 = bme2.readPressure() / 100;

  temperature2_averaged += temperature2 / _averages;                // collect temp, rh, and humidity data (averaged if API requests averaging)
  humidity2_averaged += humidity2 / _averages;
  pressure2_averaged += pressure2 / _averages;

  }
*/

// collect a light reading and print it
// assume 5V power is on

void get_detector_value(int averages, int light, int intensity, int detector, int pulsesize, int detector_read1or2or3) {  // read reflectance of LED 5 (940nm) with 5 pulses and averages.  Keep # of pulses low as a large number of pulses could impact the sample

  const unsigned STABILIZE = 10;     // this usec delay gives the LED current controller op amp the time needed to stabilize - low intensity may need more
  const unsigned BEFORE_COUNT = 19;  // # of readings to take for a baseline (max 100)
  const unsigned AFTER_COUNT = 19;   // # of readings to take with light on (max 100)

  int16_t data[averages];

  //DAC_set(light, par_to_dac(intensity, light));   // set the DAC (intensity), make sure to convert PAR intensity to DAC value
  DAC_set(light, intensity);  // set the DAC, currently not converted from PAR - set only to raw DAC value
  DAC_change();

  AD_set();
  delay(1);

  for (uint16_t i = 0; i < averages; i++) {

    int32_t baselines[100];
    int32_t values[100];

    noInterrupts();
    AD_read_array(baselines, detector, BEFORE_COUNT);  // read baseline values and save them
    digitalWriteFast(PULSE_OFF, HIGH);                 // turn on measuring light
    delayMicroseconds(STABILIZE);                      // stabilize LED current controller
    AD_read_array(values, detector, AFTER_COUNT);      // read lighted values and save them
    digitalWriteFast(PULSE_OFF, LOW);                  // turn off measuring light
    interrupts();

    data[i] = mediani(values, AFTER_COUNT) - mediani(baselines, BEFORE_COUNT);  // save result

    delayMicroseconds(pulsesize);  // let LED and current controller cool down

  }  // for

  DAC_set(light, 0);  // set the DAC off

  Serial_Printf("%d,\n", median16i(data, averages));  // the median result
  Serial_Printf("single pulse stdev = % .2f AD counts\n", stdev16i(data, averages));


#if 0  // no idea what this code is for
  if (detector_read1or2or3 == 1) {                                  // save in detector_read1 or 2 depending on which is called
    detector_read1 = median16(thisData, 4);                         // using median - 25% improvements over using mean to determine this value
    detector_read1_averaged += detector_read1 / _averages;
    //    Serial_Printf("read1: %f\n",detector_read1);
  }
  else if (detector_read1or2or3 == 2) {
    detector_read2 = median16(thisData, 4);                         // using median - 25% improvements over using mean to determine this value
    detector_read2_averaged += detector_read2 / _averages;
    //    Serial_Printf("read2: %f\n",detector_read2);
  }
  else if (detector_read1or2or3 == 3) {
    detector_read3 = median16(thisData, 4);                         // using median - 25% improvements over using mean to determine this value
    detector_read3_averaged += detector_read3 / _averages;
    //    Serial_Printf("read3: %f\n",detector_read3);
  }
#endif


}  // get_detector_value()

// void get_detector_value_test(int _averages, int this_pulses, int this_intensity, int this_detector, int this_pulsesize, int this_pulse_distance) {  // read reflectance of LED 5 (940nm) with 5 pulses and averages.  Keep # of pulses low as a large number of pulses could impact the sample
//   //  /*
//   turn_on_5V();
//   delay(100);  // probably not required but just in case...
//   int led_order[] = { 5, 9, 6, 7, 8 };
//   const unsigned STABILIZE = 10;  // this delay gives the LED current controller op amp the time needed to stabilize
//   AD_set();

//   Serial.printf("---------------\nsamples: %d, intensity: %d, detector: %d\n", this_pulses, this_intensity, this_detector);

//   for (int j = 0; j < 5; j++) {
//     int this_light = led_order[j];
//     Serial_Printf("LGT:%d, ", this_light);
//     DAC_set(this_light, this_intensity);  // set the DAC, currently not converted from PAR - set only to raw DAC value
//     int16_t thisData1[this_pulses];

// #define PULSES 20

//     for (int16_t i = 0; i < this_pulses; i++) {
//       int32_t this_sample_adc[PULSES];         // initialize the variables to hold the main and detector data
//       delayMicroseconds(this_pulse_distance);  // wait until next pulse
//       noInterrupts();
//       digitalWriteFast(PULSE_OFF, HIGH);  // turn on measuring light
//       delayMicroseconds(STABILIZE);       // this delay gives the LED current controller op amp the time needed to turn
//       // the light on completely + stabilize.
//       // Very low intensity measuring pulses may require an even longer delay here.
//       digitalWriteFast(HOLDM, LOW);       // turn off sample and hold discharge
//       delayMicroseconds(this_pulsesize);  // pulse width
// #ifndef TIA
//       digitalWriteFast(PULSE_OFF, LOW);  // turn off measuring light
// #endif
// #ifdef TIA
//       digitalWriteFast(PULSE_OFF, LOW);  // turn off measuring light
// #endif
//       interrupts();  // re-enable interrupts (left off after LED ISR)

//       AD_read_array(this_sample_adc, this_detector, PULSES);  // read values into array
//       //      Serial_Print("single pulse: ");
//       /*
//         Serial.println(micros());
//         for (int i = 0; i < PULSES; i++) {
//         Serial.print(this_sample_adc[i]);
//         Serial.print(",");
//         }
//         Serial.println(micros());
//       */
//       thisData1[i] = mediani(this_sample_adc, PULSES);  // store median
//       digitalWriteFast(HOLDM, HIGH);                    // discharge integrators
//     }                                                   // for

//     DAC_set(this_light, 0);  // turn off

//     // now report values

//     Serial_Print("det_values:");
//     for (int i = 0; i < this_pulses; ++i)
//       Serial_Printf("%d ", thisData1[i]);
//     Serial_Printf("\nmedian across pulses = %d\n", median16i(thisData1, this_pulses));
//     Serial_Printf("stdev across pulses = %.2f\n", stdev16i(thisData1, this_pulses));

//     Serial_Print_Line("");
//     /*
//       ////////////////////// DAC DROOP TEST //////////////////
//       Serial_Print("OPT1:");
//       for (int z = 0; z < 20; z++) {
//       Serial_Printf("%d,",onePoint0[z]);
//       }
//       Serial_Print_Line("");
//       ///////////////////////////////////////////////////////
//     */
//     //        Serial_Printf("all_reads_1: %f\n",this_detector_read1);
//     //    Serial_Printf("all_reads_averaged_1: %f\n",this_detector_read_averaged1);
//   }
//   //  */



// }  // get_detector_value_test()

// float get_contactless_temp (int _averages) {
//   contactless_temp = (MLX90615_Read(0) + MLX90615_Read(0) + MLX90615_Read(0)) / 3;
//   contactless_temp_averaged += contactless_temp / _averages;

//   return contactless_temp;
// }

// read accelerometer

/*
  void get_compass_and_angle (int notRaw, int _averages) {

  int magX, magY, magZ, accX, accY, accZ;
  MAG3110_read(&magX, &magY, &magZ);
  MMA8653FC_read(&accX, &accY, &accZ);

  float mag_coords[3] = {(float) magX, (float) magY, (float) magZ};
  applyMagCal(mag_coords);

  int acc_coords[3] = {accX, accY, accZ};
  applyAccCal(acc_coords);

  roll = getRoll(acc_coords[1], acc_coords[2]);
  pitch = getPitch(acc_coords[0], acc_coords[1], acc_coords[2], roll);
  compass = getCompass(mag_coords[0], mag_coords[1], mag_coords[2], pitch, roll);

  Tilt deviceTilt = calculateTilt(roll, pitch, compass);

  rad_to_deg(&roll, &pitch, &compass);

  angle = deviceTilt.angle;
  //Serial_Printf("%f \n", angle);
  angle_direction = deviceTilt.angle_direction;

  if (notRaw == 0) {                                              // save the raw values average
    x_tilt_averaged += (float)x_tilt / _averages;
    y_tilt_averaged += (float)y_tilt / _averages;
    z_tilt_averaged += (float)z_tilt / _averages;
    x_compass_raw_averaged += (float)x_compass_raw / _averages;
    y_compass_raw_averaged += (float)y_compass_raw / _averages;
    z_compass_raw_averaged += (float)z_compass_raw / _averages;
  }
  if (notRaw == 1) {                                              // save the calibrated values and average
    roll_averaged += roll / _averages;
    pitch_averaged += pitch / _averages;
    compass_averaged += compass / _averages;
    angle_averaged += angle / _averages;
  }
  // add better routine here to produce clearer tilt values
  }
*/

// read the hall sensor to measure thickness of a leaf

/*
  float get_thickness (int notRaw, int _averages) {
  int sum = 0;
  for (int i = 0; i < 1000; ++i) {
    sum += analogRead(HALL_OUT);
  }
  thickness_raw = (sum / 1000);
  // dividing thickness_a by 1,000,000,000 to bring it back to it's actual value
  thickness = (eeprom->thickness_a * thickness_raw * thickness_raw / 1000000000 + eeprom->thickness_b * thickness_raw + eeprom->thickness_c) / 1000; // calibration information is saved in uM, so divide by 1000 to convert back to mm.

  if (notRaw == 0) {                                              // save the raw values average
    thickness_raw_averaged += (float)thickness_raw / _averages;
    return thickness_raw;
  }
  else if (notRaw == 1) {                                              // save the calibrated values and average
    thickness_averaged += (float)thickness / _averages;
    return thickness;
  }
  else {
    return 0;
  }
  }
*/

float get_analog_read(int pin, int _averages) {
  int sum = 0;
  for (int i = 0; i < 1000; ++i) {
    sum += analogRead(pin);
  }
  analog_read = (sum / 1000);
  analog_read_averaged += (float)analog_read / _averages;
  return analog_read;
}



// float get_co2(int _averages) {
//   co2 = requestCo2(2000);
//   //  co2 = getCo2(response);
//   delay(100);
//   co2_averaged += (float) co2 / _averages;
// #ifdef DEBUGSIMPLE
//   Serial_Print("\"co2_content\":");
//   Serial_Print(co2_averaged);
//   Serial_Print(",");
// #endif
//   return co2;
// }

float get_digital_read(int pin, int _averages) {
  digital_read = digitalRead(pin);
  digital_read_averaged += (float)digital_read / _averages;
  return analog_read;
}

// float get_adc_read (int adc_channel, int _averages) {       // adc channels 1 - 4 available through USB 3.0 port.  Actual channels are 4 - 7, so take user value and add 3
//   adc_read = AD7689_read(adc_channel + 3);
//   adc_read_averaged += (float) adc_read / _averages;
//   return adc_read;
// }
// float get_adc_read2 (int adc_channel, int _averages) {       // adc channels 1 - 4 available through USB 3.0 port.  Actual channels are 4 - 7, so take user value and add 3
//   adc_read2 = AD7689_read(adc_channel + 3);
//   adc_read2_averaged += (float) adc_read2 / _averages;
//   return adc_read2;
// }
// float get_adc_read3 (int adc_channel, int _averages) {       // adc channels 1 - 4 available through USB 3.0 port.  Actual channels are 4 - 7, so take user value and add 3
//   adc_read3 = AD7689_read(adc_channel + 3);
//   adc_read3_averaged += (float) adc_read3 / _averages;
//   return adc_read3;
// }

// check for commands to read various envirmental sensors
// also output the value on the final call

// TODO ok instead of just adding up the values in the array, we probably need to make another nest in the nested array, otherwise we don't know where to start the next environmentals... so right

static void environmentals(JsonArray environmental, const int _averages, const int count, int oneOrArray) {
  /*
    if (oneOrArray == 0) {                                     // always measure temperature so long as it's not an array
    get_temperature_humidity_pressure_voc(_averages);
    if (count == _averages - 1) {
      Serial_Printf("\"temperature\":%f,\"humidity\":%f,\"pressure\":%f,\"voc\":%f,", temperature_averaged, humidity_averaged, pressure_averaged, voc_averaged);
    }
    }
  */

  for (int i = 0; i < environmental.getLength(); i++) {  // call environmental measurements after the spectroscopic measurement

    String thisSensor = environmental.getArray(i).getString(0);
    //    Serial_Printf("thisSensor: %s", thisSensor);

    //    if (oneOrArray == 1 && thisSensor == "temperature_humidity_pressure_voc") {                 // if it is an array, then only measure it if it's requested.  measure contactless temperature, but only evaluate here if it's environmental_array, otherwise it evaluates it all the time automatically!
    if (thisSensor == "temperature_humidity_pressure_voc") {  // if it is an array, then only measure it if it's requested.  measure contactless temperature, but only evaluate here if it's environmental_array, otherwise it evaluates it all the time automatically!
      get_temperature_humidity_pressure_voc(_averages);
      if (count == _averages - 1 && oneOrArray == 0) {
        Serial_Printf("\"temperature\":%f,\"humidity\":%f,\"pressure\":%f,\"voc\":%f,", temperature_averaged, humidity_averaged, pressure_averaged, voc_averaged);
      }
    }

    // else if (thisSensor == "contactless_temp") {                 // measure contactless temperature
    //   get_contactless_temp(_averages);
    //   if (count == _averages - 1 && oneOrArray == 0) {
    //     Serial_Printf("\"contactless_temp\":%.2f,", contactless_temp_averaged);
    //   }
    // }

    /*

      else if (thisSensor == "light_intensity") {                   // measure light intensity with par calibration applied
      get_light_intensity(_averages);
      if (count == _averages - 1 && oneOrArray == 0) {
        Serial_Printf("\"light_intensity\":%.2f,\"r\":%.2f,\"g\":%.2f,\"b\":%.2f,\"light_intensity_raw\":%.2f,", light_intensity_averaged, r_averaged, g_averaged, b_averaged, light_intensity_raw_averaged);
      }
      }

      else if (thisSensor == "thickness") {                        // measure thickness via hall sensor, with calibration applied
      get_thickness(1, _averages);
      if (count == _averages - 1 && oneOrArray == 0) {
        Serial_Printf("\"thickness\":%.2f,", thickness_averaged);
      }
      }

      else if (thisSensor == "thickness_raw") {                    // measure thickness via hall sensor, raw con
      get_thickness(0, _averages);
      if (count == _averages - 1 && oneOrArray == 0) {
        Serial_Printf("\"thickness_raw\":%.2f,", thickness_raw_averaged);
      }
      }
        else if (thisSensor == "compass_and_angle") {                             // measure tilt in -180 - 180 degrees
          get_compass_and_angle(1, _averages);
          if (count == _averages - 1 && oneOrArray == 0) {
            Serial_Printf("\"compass_direction\":%s,\"compass\":\"%.2f\",\"angle\":%.2f,\"angle_direction\":%s,\"pitch\":%.2f,\"roll\":%.2f,", getDirection(compass_segment(compass_averaged)), compass_averaged, angle_averaged, angle_direction.c_str(), pitch_averaged, roll_averaged);
          }
        }

        else if (thisSensor == "compass_and_angle_raw") {                         // measure tilt from -1000 - 1000
          get_compass_and_angle(0, _averages);
          if (count == _averages - 1 && oneOrArray == 0) {
            Serial_Printf("\"x_tilt\":%.2f,\"y_tilt\":%.2f,\"z_tilt\":%.2f,\"x_compass_raw\":%.2f,\"y_compass_raw\":%.2f,\"z_compass_raw\":%.2f,", x_tilt_averaged, y_tilt_averaged, z_tilt_averaged, x_compass_raw_averaged, y_compass_raw_averaged, z_compass_raw_averaged);
          }
        }
    */
    else if (thisSensor == "detector_read1") {
      int this_light = environmental.getArray(i).getLong(1);
      int this_intensity = environmental.getArray(i).getLong(2);
      int this_detector = environmental.getArray(i).getLong(3);
      int this_pulsesize = environmental.getArray(i).getLong(4);
      get_detector_value(_averages, this_light, this_intensity, this_detector, this_pulsesize, 1);  // save as "detector_read1" from get_detector_value function
      if (count == _averages - 1 && oneOrArray == 0) {
        Serial_Printf("\"detector_read1\":%f,", detector_read1_averaged);
      }
    }

    else if (thisSensor == "detector_read2") {
      int this_light = environmental.getArray(i).getLong(1);
      int this_intensity = environmental.getArray(i).getLong(2);
      int this_detector = environmental.getArray(i).getLong(3);
      int this_pulsesize = environmental.getArray(i).getLong(4);
      get_detector_value(_averages, this_light, this_intensity, this_detector, this_pulsesize, 2);  // save as "detector_read2" from get_detector_value function
      if (count == _averages - 1 && oneOrArray == 0) {
        Serial_Printf("\"detector_read2\":%f,", detector_read2_averaged);
      }
    }

    else if (thisSensor == "detector_read3") {
      int this_light = environmental.getArray(i).getLong(1);
      int this_intensity = environmental.getArray(i).getLong(2);
      int this_detector = environmental.getArray(i).getLong(3);
      int this_pulsesize = environmental.getArray(i).getLong(4);
      get_detector_value(_averages, this_light, this_intensity, this_detector, this_pulsesize, 3);  // save as "detector_read3" from get_detector_value function
      if (count == _averages - 1 && oneOrArray == 0) {
        Serial_Printf("\"detector_read3\":%f,", detector_read3_averaged);
      }
    }

    else if (thisSensor == "analog_read") {  // perform analog reads
      int pin = environmental.getArray(i).getLong(1);
      get_analog_read(pin, _averages);
      if (count == _averages - 1 && oneOrArray == 0) {
        Serial_Printf("\"analog_read\":%f,", analog_read_averaged);
      }
    }

    else if (thisSensor == "digital_read") {  // perform digital reads
      int pin = environmental.getArray(i).getLong(1);
      get_digital_read(pin, _averages);
      if (count == _averages - 1 && oneOrArray == 0) {
        Serial_Printf("\"digital_read\":%f,", digital_read_averaged);
      }
    }

    // else if (thisSensor == "co2") {                            // perform digital reads
    //   get_co2(_averages);
    //   if (count == _averages - 1 && oneOrArray == 0) {
    //     Serial_Printf("\"co2\":%f,", co2_averaged);
    //   }
    // }

    // else if (thisSensor == "adc_read") {                      // perform digital reads
    //   int adc_channel = environmental.getArray(i).getLong(1);
    //   get_adc_read(adc_channel, _averages);
    //   if (count == _averages - 1 && oneOrArray == 0) {
    //     Serial_Printf("\"adc_read\":%f,", adc_read_averaged);
    //   }
    // }

    // else if (thisSensor == "adc_read2") {                      // perform digital reads
    //   int adc_channel = environmental.getArray(i).getLong(1);
    //   get_adc_read2(adc_channel, _averages);
    //   if (count == _averages - 1 && oneOrArray == 0) {
    //     Serial_Printf("\"adc_read2\":%f,", adc_read2_averaged);
    //   }
    // }

    // else if (thisSensor == "adc_read3") {                      // perform digital reads
    //   int adc_channel = environmental.getArray(i).getLong(1);
    //   get_adc_read3(adc_channel, _averages);
    //   if (count == _averages - 1 && oneOrArray == 0) {
    //     Serial_Printf("\"adc_read3\":%f,", adc_read3_averaged);
    //   }
    // }

    else if (thisSensor == "digital_write") {  // perform digital write
      int pin = environmental.getArray(i).getLong(1);
      int setting = environmental.getArray(i).getLong(2);
      Serial_Flush_Input();
      pinMode(pin, OUTPUT);
      //      delayMicroseconds(300);
      digitalWriteFast(pin, setting);
    }

    else if (thisSensor == "analog_write") {  // perform analog write with length of time to apply the pwm
      int pin = environmental.getArray(i).getLong(1);
      int setting = environmental.getArray(i).getLong(2);
      int resolution = environmental.getArray(i).getLong(3);
      int wait = environmental.getArray(i).getLong(4);

      // create lookup table for resolution and frequency settings (values start at position 2 for a resolution of 2):
      float freq_table[] = { 0, 0, 15000000, 7500000, 3750000, 1875000, 937500, 468750, 234375, 117187.5, 58593.75, 29296.875, 14648.437, 7324.219, 3662.109, 1831.055, 915.527 };

      //      Serial_Printf("pin = %d, setting = %d, resolution = %d, wait = %d",pin, setting, resolution, wait);

      // TODO sanity checks

      pinMode(pin, OUTPUT);
      analogWriteFrequency(pin, freq_table[resolution]);  // set analog frequency
      analogWriteResolution(resolution);                  // set analog frequency
      analogWrite(pin, setting);
      delay(wait);
      analogWrite(pin, 0);
      analogWriteResolution(12);  // reset analog resolution
    }                             // if
  }                               // for
}  //environmentals()

static void print_all() {
  // print every value saved in eeprom in valid json structure (even the undefined values which are still 0)
  Serial_Printf("light_intensity:%g\n", light_intensity);
}

#if 0
// ??  why
void reset_freq() {
  analogWriteFrequency(5, 187500);                                               // reset timer 0
  analogWriteFrequency(3, 187500);                                               // reset timer 1
  analogWriteFrequency(25, 488.28);                                              // reset timer 2
  /*
    Teensy 3.0              Ideal Freq:
    16      0 - 65535       732 Hz          366 Hz
    15      0 - 32767       1464 Hz         732 Hz
    14      0 - 16383       2929 Hz         1464 Hz
    13      0 - 8191        5859 Hz         2929 Hz
    12      0 - 4095        11718 Hz        5859 Hz
    11      0 - 2047        23437 Hz        11718 Hz
    10      0 - 1023        46875 Hz        23437 Hz
    9       0 - 511         93750 Hz        46875 Hz
    8       0 - 255         187500 Hz       93750 Hz
    7       0 - 127         375000 Hz       187500 Hz
    6       0 - 63          750000 Hz       375000 Hz
    5       0 - 31          1500000 Hz      750000 Hz
    4       0 - 15          3000000 Hz      1500000 Hz
    3       0 - 7           6000000 Hz      3000000 Hz
    2       0 - 3           12000000 Hz     6000000 Hz

  */
}
#endif

void print_calibrations() {
  unsigned i;

  // check battery before proceeding
  int batteryPercent = battery_percent(0);  // percent with no load
#ifdef YESBATT
  if (batteryPercent < 10) {
    Serial_Printf("{\"error\":\"Battery critically low at %d %%, unable to collect data.  Charge device via micro USB.\"}", batteryPercent);
    Serial_Print_CRC();
    Serial_Flush_Output();
    return;
  }
#endif

  Serial_Printf("{\"device_name\":\"%s\",\"device_version\":\"%s\",\"device_id\":\"%2.2x:%2.2x:%2.2x:%2.2x\",\"device_battery\":%d,\"device_firmware\":\"%s\",\"shutdown\":%d,\n", DEVICE_NAME, DEVICE_VERSION,
                (unsigned)eeprom->device_id >> 24,
                ((unsigned)eeprom->device_id & 0xff0000) >> 16,
                ((unsigned)eeprom->device_id & 0xff00) >> 8,
                (unsigned)eeprom->device_id & 0xff,
                batteryPercent,
                DEVICE_FIRMWARE,
                (unsigned)eeprom->shutdownTime);
  //  Serial_Printf("\"mag_bias\": [\"%f\",\"%f\",\"%f\"],\n", eeprom->mag_bias[0], eeprom->mag_bias[1], eeprom->mag_bias[2]);
  //  Serial_Printf("\"mag_cal\": [[\"%f\",\"%f\",\"%f\"],[\"%f\",\"%f\",\"%f\"],[\"%f\",\"%f\",\"%f\"]],\n", eeprom->mag_cal[0][0], eeprom->mag_cal[0][1], eeprom->mag_cal[0][2], eeprom->mag_cal[1][0], eeprom->mag_cal[1][1], eeprom->mag_cal[1][2], eeprom->mag_cal[2][0], eeprom->mag_cal[2][1], eeprom->mag_cal[2][2]);
  //  Serial_Printf("\"accel_bias\": [\"%f\",\"%f\",\"%f\"],\n", eeprom->accel_bias[0], eeprom->accel_bias[1], eeprom->accel_bias[2]);
  //  Serial_Printf("\"accel_cal\": [[\"%f\",\"%f\",\"%f\"],[\"%f\",\"%f\",\"%f\"],[\"%f\",\"%f\",\"%f\"]],\n", eeprom->accel_cal[0][0], eeprom->accel_cal[0][1], eeprom->accel_cal[0][2], eeprom->accel_cal[1][0], eeprom->accel_cal[1][1], eeprom->accel_cal[1][2], eeprom->accel_cal[2][0], eeprom->accel_cal[2][1], eeprom->accel_cal[2][2]);
  //  Serial_Printf("\"light_slope_all\": \"%f\",\n", eeprom->light_slope_all);
  //  Serial_Printf("\"light_slope_r\": \"%f\",\n", eeprom->light_slope_r);
  //  Serial_Printf("\"light_slope_g\": \"%f\",\n", eeprom->light_slope_g);
  //  Serial_Printf("\"light_slope_b\": \"%f\",\n", eeprom->light_slope_b);
  //  Serial_Printf("\"light_yint\": \"%f\",\n", eeprom->light_yint);
  //  Serial_Printf("\"detector_offset_slope\": [\"%f\",\"%f\",\"%f\",\"%f\"],\n", eeprom->detector_offset_slope[0], eeprom->detector_offset_slope[1], eeprom->detector_offset_slope[2], eeprom->detector_offset_slope[3]);
  //  Serial_Printf("\"detector_offset_yint\": [\"%f\",\"%f\",\"%f\",\"%f\"],\n", eeprom->detector_offset_yint[0], eeprom->detector_offset_yint[1], eeprom->detector_offset_yint[2], eeprom->detector_offset_yint[3]);
  //  Serial_Printf("\"thickness_a\": \"%f\",\n", eeprom->thickness_a);
  //  Serial_Printf("\"thickness_b\": \"%f\",\n", eeprom->thickness_b);
  //  Serial_Printf("\"thickness_c\": \"%f\",\n", eeprom->thickness_c);
  //  Serial_Printf("\"thickness_min\": \"%f\",\n", eeprom->thickness_min);
  //  Serial_Printf("\"thickness_max\": \"%f\",\n", eeprom->thickness_max);


  // REFLECTOMETER Changed some of standard multispeq EEPROM save names, specifically -->
  /*
     used to be --> changed to
    par_to_dac_slope1 --> ir_slope
    par_to_dac_slope2 --> ir_yint
    par_to_dac_slope3 --> vis_slope
    par_to_dac_slope4 --> vis_yint
    par_to_dac_yint --> ir_blank
    ir_baseline_slope --> vis_blank
    ir_baseline_yint --> no change
    // Can be changed back by changing here, and reinstating in defines.h, PAR.cpp, and eeprom.h
  */

  Serial_Print("\"ir_high\": [");
  for (i = 0; i < arraysize(eeprom->ir_high) - 1; i++)
    Serial_Printf("\"%f\",", eeprom->ir_high[i]);
  Serial_Printf("\"%f\"],\n", eeprom->ir_high[i]);

  Serial_Print("\"ir_low\": [");
  for (i = 0; i < arraysize(eeprom->ir_low) - 1; i++)
    Serial_Printf("\"%f\",", eeprom->ir_low[i]);
  Serial_Printf("\"%f\"],\n", eeprom->ir_low[i]);

  Serial_Print("\"vis_high\": [");
  for (i = 0; i < arraysize(eeprom->vis_high) - 1; i++)
    Serial_Printf("\"%f\",", eeprom->vis_high[i]);
  Serial_Printf("\"%f\"],\n", eeprom->vis_high[i]);

  Serial_Print("\"vis_low\": [");
  for (i = 0; i < arraysize(eeprom->vis_low) - 1; i++)
    Serial_Printf("\"%f\",", eeprom->vis_low[i]);
  Serial_Printf("\"%f\"],\n", eeprom->vis_low[i]);

  Serial_Print("\"ir_high_master\": [");
  for (i = 0; i < arraysize(eeprom->ir_high_master) - 1; i++)
    Serial_Printf("\"%f\",", eeprom->ir_high_master[i]);
  Serial_Printf("\"%f\"],\n", eeprom->ir_high_master[i]);

  Serial_Print("\"ir_low_master\": [");
  for (i = 0; i < arraysize(eeprom->ir_low_master) - 1; i++)
    Serial_Printf("\"%f\",", eeprom->ir_low_master[i]);
  Serial_Printf("\"%f\"],\n", eeprom->ir_low_master[i]);

  Serial_Print("\"vis_high_master\": [");
  for (i = 0; i < arraysize(eeprom->vis_high_master) - 1; i++)
    Serial_Printf("\"%f\",", eeprom->vis_high_master[i]);
  Serial_Printf("\"%f\"],\n", eeprom->vis_high_master[i]);

  Serial_Print("\"vis_low_master\": [");
  for (i = 0; i < arraysize(eeprom->vis_low_master) - 1; i++)
    Serial_Printf("\"%f\",", eeprom->vis_low_master[i]);
  Serial_Printf("\"%f\"],\n", eeprom->vis_low_master[i]);

  Serial_Print("\"ir_blank\": [");
  for (i = 0; i < arraysize(eeprom->ir_blank) - 1; i++)
    Serial_Printf("\"%f\",", eeprom->ir_blank[i]);
  Serial_Printf("\"%f\"],\n", eeprom->ir_blank[i]);

  Serial_Print("\"vis_blank\": [");
  for (i = 0; i < arraysize(eeprom->vis_blank) - 1; i++)
    Serial_Printf("\"%f\",", eeprom->vis_blank[i]);
  Serial_Printf("\"%f\"],\n", eeprom->vis_blank[i]);

  Serial_Print("\"heatcal1\": [");
  for (i = 0; i < arraysize(eeprom->heatcal1) - 1; i++)
    Serial_Printf("\"%f\",", eeprom->heatcal1[i]);
  Serial_Printf("\"%f\"],\n", eeprom->heatcal1[i]);

  Serial_Print("\"heatcal2\": [");
  for (i = 0; i < arraysize(eeprom->heatcal2) - 1; i++)
    Serial_Printf("\"%f\",", eeprom->heatcal2[i]);
  Serial_Printf("\"%f\"],\n", eeprom->heatcal2[i]);

  Serial_Print("\"heatcal3\": [");
  for (i = 0; i < arraysize(eeprom->heatcal3) - 1; i++)
    Serial_Printf("\"%f\",", eeprom->heatcal3[i]);
  Serial_Printf("\"%f\"],\n", eeprom->heatcal3[i]);

  /*
    Serial_Print("\"heatcal4\": [");
    for (i = 0; i < arraysize(eeprom->heatcal4) - 1; i++)
      Serial_Printf("\"%f\",", eeprom->heatcal4[i]);
    Serial_Printf("\"%f\"],\n", eeprom->heatcal4[i]);

    Serial_Print("\"heatcal5\": [");
    for (i = 0; i < arraysize(eeprom->heatcal5) - 1; i++)
      Serial_Printf("\"%f\",", eeprom->heatcal5[i]);
    Serial_Printf("\"%f\"],\n", eeprom->heatcal5[i]);
  */


  /*  DEPRECIATED
    Serial_Print("\"ir_baseline_yint\": [");
    for (i = 0; i < arraysize(eeprom->ir_baseline_yint) - 1; i++)
      Serial_Printf("\"%f\",", eeprom->ir_baseline_yint[i]);
    Serial_Printf("\"%f\"],\n", eeprom->ir_baseline_yint[i]);

    Serial_Print("\"colorcal_intensity1_slope\": [");
    for (i = 0; i < arraysize(eeprom->colorcal_intensity1_slope) - 1; i++)
      Serial_Printf("\"%f\",", eeprom->colorcal_intensity1_slope[i]);
    Serial_Printf("\"%f\"],\n", eeprom->colorcal_intensity1_slope[i]);

    Serial_Print("\"colorcal_intensity1_yint\": [");
    for (i = 0; i < arraysize(eeprom->colorcal_intensity1_yint) - 1; i++)
      Serial_Printf("\"%f\",", eeprom->colorcal_intensity1_yint[i]);
    Serial_Printf("\"%f\"],\n", eeprom->colorcal_intensity1_yint[i]);

    Serial_Print("\"colorcal_intensity2_slope\": [");
    for (i = 0; i < arraysize(eeprom->colorcal_intensity2_slope) - 1; i++)
      Serial_Printf("\"%f\",", eeprom->colorcal_intensity2_slope[i]);
    Serial_Printf("\"%f\"],\n", eeprom->colorcal_intensity2_slope[i]);
    Serial_Print("\"colorcal_intensity2_yint\": [");
    for (i = 0; i < arraysize(eeprom->colorcal_intensity2_yint) - 1; i++)
      Serial_Printf("\"%f\",", eeprom->colorcal_intensity2_yint[i]);
    Serial_Printf("\"%f\"],\n", eeprom->colorcal_intensity2_yint[i]);

    Serial_Print("\"colorcal_intensity3_slope\": [");
    for (i = 0; i < arraysize(eeprom->colorcal_intensity3_slope) - 1; i++)
      Serial_Printf("\"%f\",", eeprom->colorcal_intensity3_slope[i]);
    Serial_Printf("\"%f\"],\n", eeprom->colorcal_intensity3_slope[i]);
    Serial_Print("\"colorcal_intensity3_yint\": [");
    for (i = 0; i < arraysize(eeprom->colorcal_intensity3_yint) - 1; i++)
      Serial_Printf("\"%f\",", eeprom->colorcal_intensity3_yint[i]);
    Serial_Printf("\"%f\"],\n", eeprom->colorcal_intensity3_yint[i]);
  */
  // Serial_Print("\"colorcal_blank1\": [");
  // for (i = 0; i < arraysize(eeprom->colorcal_blank1) - 1; i++)
  //   Serial_Printf("\"%f\",", eeprom->colorcal_blank1[i]);
  // Serial_Printf("\"%f\"],\n", eeprom->colorcal_blank1[i]);
  // Serial_Print("\"colorcal_blank2\": [");
  // for (i = 0; i < arraysize(eeprom->colorcal_blank2) - 1; i++)
  //   Serial_Printf("\"%f\",", eeprom->colorcal_blank2[i]);
  // Serial_Printf("\"%f\"],\n", eeprom->colorcal_blank2[i]);
  // Serial_Print("\"colorcal_blank3\": [");
  // for (i = 0; i < arraysize(eeprom->colorcal_blank3) - 1; i++)
  //   Serial_Printf("\"%f\",", eeprom->colorcal_blank3[i]);
  // Serial_Printf("\"%f\"],\n", eeprom->colorcal_blank3[i]);
  Serial_Print("\"normalized_soil\": [");
  for (i = 0; i < arraysize(eeprom->normalized_soil) - 1; i++)
    Serial_Printf("\"%f\",", eeprom->normalized_soil[i]);
  Serial_Printf("\"%f\"],\n", eeprom->normalized_soil[i]);
  Serial_Print("\"normalized_5mlcuvette\": [");
  for (i = 0; i < arraysize(eeprom->normalized_5mlcuvette) - 1; i++)
    Serial_Printf("\"%f\",", eeprom->normalized_5mlcuvette[i]);
  Serial_Printf("\"%f\"],\n", eeprom->normalized_5mlcuvette[i]);
  Serial_Print("\"normalized_1mlcuvette\": [");
  for (i = 0; i < arraysize(eeprom->normalized_1mlcuvette) - 1; i++)
    Serial_Printf("\"%f\",", eeprom->normalized_1mlcuvette[i]);
  Serial_Printf("\"%f\"],\n", eeprom->normalized_1mlcuvette[i]);
  Serial_Print("\"normalized_custom\": [");
  for (i = 0; i < arraysize(eeprom->normalized_custom) - 1; i++)
    Serial_Printf("\"%f\",", eeprom->normalized_custom[i]);
  Serial_Printf("\"%f\"],\n", eeprom->normalized_custom[i]);

  for (i = 0; i < NUM_USERDEFS - 1; i++)
    Serial_Printf("\"userdef%d\": \"%f\",\n", i, eeprom->userdef[i]);
  Serial_Printf("\"userdef%d\": \"%f\"\n}", i, eeprom->userdef[i]);

  Serial_Print_CRC();
}

theReadings getReadings(const char* _thisSensor) {  // get the actual sensor readings associated with each environmental call (so compass and angle when you call compass_and_angle, etc.)
  theReadings _theReadings;
  if (!strcmp(_thisSensor, "temperature_humidity_pressure_voc")) {
    _theReadings.reading1 = "temperature";
    _theReadings.reading2 = "humidity";
    _theReadings.reading3 = "pressure";
    _theReadings.reading4 = "voc";
    _theReadings.numberReadings = 4;
  } else if (!strcmp(_thisSensor, "analog_read")) {
    _theReadings.reading1 = "analog_read";
    _theReadings.numberReadings = 1;
  }
  // else if (!strcmp(_thisSensor, "adc_read")) {
  //   _theReadings.reading1 = "adc_read";
  //   _theReadings.numberReadings = 1;
  // }
  // else if (!strcmp(_thisSensor, "adc_read2")) {
  //   _theReadings.reading1 = "adc_read2";
  //   _theReadings.numberReadings = 1;
  // }
  // else if (!strcmp(_thisSensor, "adc_read3")) {
  //   _theReadings.reading1 = "adc_read3";
  //   _theReadings.numberReadings = 1;
  // }
  // else if (!strcmp(_thisSensor, "co2")) {
  //   _theReadings.reading1 = "co2";
  //   _theReadings.numberReadings = 1;
  // }
  /*
    else if (!strcmp(_thisSensor, "temperature_humidity_pressure2")) {
    _theReadings.reading1 = "temperature2";
    _theReadings.reading2 = "humidity2";
    _theReadings.reading3 = "pressure2";
    _theReadings.numberReadings = 3;
    }
    else if (!strcmp(_thisSensor, "thickness")) {
    _theReadings.reading1 = "thickness";
    _theReadings.reading2 = "thickness_raw";
    _theReadings.numberReadings = 2;
    }
    else if (!strcmp(_thisSensor, "light_intensity")) {
    _theReadings.reading1 = "light_intensity";
    _theReadings.reading2 = "light_intensity_raw";
    _theReadings.reading3 = "r";
    _theReadings.reading4 = "g";
    _theReadings.reading5 = "b";
    _theReadings.numberReadings = 5;
    }  */
  // else if (!strcmp(_thisSensor, "contactless_temp")) {
  //   _theReadings.reading1 = "contactless_temp";
  //   _theReadings.numberReadings = 1;
  // }
  // else if (!strcmp(_thisSensor, "compass_and_angle")) {
  //   _theReadings.reading1 = "compass";
  //   _theReadings.reading2 = "angle";
  //   _theReadings.numberReadings = 2;
  // }
  else if (!strcmp(_thisSensor, "digital_read")) {
    _theReadings.reading1 = "digital_read";
    _theReadings.numberReadings = 1;
  } else if (!strcmp(_thisSensor, "detector_read1")) {
    _theReadings.reading1 = "detector_read1";
    _theReadings.numberReadings = 1;
  } else if (!strcmp(_thisSensor, "detector_read2")) {
    _theReadings.reading1 = "detector_read2";
    _theReadings.numberReadings = 1;
  } else if (!strcmp(_thisSensor, "detector_read3")) {
    _theReadings.reading1 = "detector_read3";
    _theReadings.numberReadings = 1;
  } else {  // output invalid entry if the entered sensor call is not on the list
    _theReadings.reading1 = "invalid_entry";
    _theReadings.numberReadings = 1;
  }
  //  Serial_Printf("%s, %s, %s, %s, %s, %d\n", _theReadings.reading1, _theReadings.reading2, _theReadings.reading3, _theReadings.reading4, _theReadings.reading5, _theReadings.numberReadings);
  return _theReadings;
}

//======================================

void get_set_device_info(const int _set) {

  if (_set == 1) {
    long val;

    // please enter new device ID (lower 4 bytes of BLE MAC address as a long int) followed by '+'
    //    Serial_Print_Line("{\"message\": \"Please enter device mac address (long int) followed by +: \"}\n");
    val = Serial_Input_Long("+", 0);  // save to eeprom
    store(device_id, val);            // save to eeprom
  }                                   // if

  // print


  // check battery before proceeding
  int batteryPercent = battery_percent(0);  // percent with no load
#ifdef YESBATT
  if (batteryPercent < 10) {
    Serial_Printf("{\"error\":\"Battery critically low at %d %%, unable to collect data.  Charge device via micro USB.\"}", batteryPercent);
    Serial_Print_CRC();
    Serial_Flush_Output();
    return;
  }
#endif
  Serial_Printf("{\"device_name\":\"%s\",\"device_version\":\"%s\",\"device_id\":\"%2.2x:%2.2x:%2.2x:%2.2x\",\"device_battery\":%d,\"device_firmware\":\"%s\",\"shutdown\":%d", DEVICE_NAME, DEVICE_VERSION,
                (unsigned)eeprom->device_id >> 24,
                ((unsigned)eeprom->device_id & 0xff0000) >> 16,
                ((unsigned)eeprom->device_id & 0xff00) >> 8,
                (unsigned)eeprom->device_id & 0xff,
                batteryPercent,
                DEVICE_FIRMWARE,
                (unsigned)eeprom->shutdownTime);
  //  Serial_Print(DEVICE_CONFIGURATION);
  Serial_Print("}");

  Serial_Print_CRC();

  return;

}  // get_set_device_info()

// ======================================
